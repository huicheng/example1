package app.beautypetsaloonsystem.models;

import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class PetProduct {
    private int petProductID;
    private String productName;
    private String quantityInStock;
    private boolean stockAvailabilityStatus;
    private boolean petProductNotInReport;
    
    public PetProduct() {}
    
    public PetProduct(List petProductList, String productName, String quantityInStock, boolean petProductNotInReport)
    {
        this.setPetProductID(petProductList);
        this.setProductName(productName);
        this.setQuantityInStock(quantityInStock);
        this.setStockAvailabilityStatus();
        this.setPetProductNotInReport(petProductNotInReport);
    }
    
    public int getPetProductID()
    {
        return this.petProductID;
    }
    
    public void setPetProductID(List<PetProduct> petProductList){
        int maxIDNum = 0;
        
        if(petProductList.isEmpty())
        {
            maxIDNum=1;
        }
        else
        {
            maxIDNum=petProductList.get(0).getPetProductID();
        
            for(PetProduct petProduct : petProductList)
            {
                if(petProduct.getPetProductID() > maxIDNum)
                {
                    maxIDNum = petProduct.getPetProductID();
                }
            }
            maxIDNum++;
        }
        
        this.petProductID=maxIDNum;
    }
    
    public String getProductName()
    {
        return this.productName;
    }
    
    public void setProductName(String productName)
    {
        if(Validation.isValidString(productName))
        {
            this.productName = productName;
        }
        else
        {
            try {
                throw new Exception();
            } catch (Exception ex) {
                System.out.println("Sorry! Product Name can only contain letters.");
            }
        }
    }
    
    public String getQuantityInStock()
    {
        return this.quantityInStock;
    }
    
    public void setQuantityInStock(String quantityInStock)
    {
        if(Validation.isValidNumber(quantityInStock))
        {
            this.quantityInStock = quantityInStock;
        }
        else
        {
            try {
                throw new Exception();
            } catch (Exception ex) {
                System.out.println("Sorry! Quantity In-Stock can only contain numbers.");
            }
        }
    }
    
    public boolean getStockAvailabilityStatus()
    {
        return this.stockAvailabilityStatus;
    }
    
    public void setStockAvailabilityStatus()
    {
        this.stockAvailabilityStatus = Integer.parseInt(this.getQuantityInStock()) != 0;
    }
    
    public boolean getPetProductNotInReport()
    {
        return this.petProductNotInReport;
    }
    
    public void setPetProductNotInReport(boolean petProductNotInReport)
    {
        this.petProductNotInReport=petProductNotInReport;
    }
    
    public String displayStockAvailability(boolean stockAvailabilityStatus)
    {
        return stockAvailabilityStatus ? "AVAILABLE" : "UNAVAILABLE";
    }
    
    public void displayPetProductDetails(List<PetProduct> petProductList)
    {
        if(!petProductList.isEmpty())
        {
            System.out.println("*************************************************** Pet Product List ***************************************************");
            System.out.format("%-20s%-20s%-20s%-20s", "ID", "Product Name", "Quantity in Stock", "Stock Status");
            System.out.println();
            
            for(PetProduct petProduct : petProductList)
            {
                System.out.format("%-20s%-20s%-20s%-20s", petProduct.getPetProductID(), petProduct.getProductName(),
                                    petProduct.getQuantityInStock(), petProduct.displayStockAvailability(petProduct.getStockAvailabilityStatus()));
                System.out.println();
            }                
            System.out.println("*************************************************** Pet Product List ***************************************************");
        }
    }
}