package app.beautypetsaloonsystem.models;

/**
 *
 * @author Kishani Joseph
 */
public class PetSitterAppointmentTaken {
    private PetSitter petSitter;
    private int appointmentTakenNo;
    
    public PetSitterAppointmentTaken(){}
    
    public PetSitter getPetSitter()
    {
        return this.petSitter;
    }
    
    public void setPetSitter(PetSitter petSitter)
    {
        this.petSitter=petSitter;
    }
    
    public int getAppointmentTakenNo()
    {
        return this.appointmentTakenNo;
    }
    
    public void setAppointmentTakenNo(int appointmentTakenNo)
    {
        this.appointmentTakenNo=appointmentTakenNo;
    }
}
