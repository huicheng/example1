package app.beautypetsaloonsystem.models;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class TimeSlotDate {
    private int timeSlotDateID;
    private TimeSlot timeSlot;
    private Date timeSlotDate;
    
    public TimeSlotDate(){}
    
    public TimeSlotDate(TimeSlot timeSlot, Date timeSlotDate)
    {
        this.setTimeSlot(timeSlot);
        this.setTimeSlotDate(timeSlotDate);
    }
    
    public int getTimeSlotDateID()
    {
        return this.timeSlotDateID;
    }
    
    public void setTimeSlotDateID(List<TimeSlotDate> dateTimeSlotList)
    {
        int maxIDNum = 0;
        
        if(dateTimeSlotList.isEmpty())
        {
            maxIDNum=1;
        }
        else
        {
            maxIDNum=dateTimeSlotList.get(0).getTimeSlotDateID();
        
            for(TimeSlotDate timeSlotDate : dateTimeSlotList)
            {
                if(timeSlotDate.getTimeSlotDateID()>maxIDNum)
                {
                    maxIDNum = timeSlotDate.getTimeSlotDateID();
                }
            }
            maxIDNum++;
        }
        
        this.timeSlotDateID=maxIDNum;
        
    }
    
    public TimeSlot getTimeSlot()
    {
        return this.timeSlot;
    }
    
    public void setTimeSlot(TimeSlot timeSlot)
    {
        this.timeSlot=timeSlot;
    }
    
    public Date getTimeSlotDate()
    {
        return this.timeSlotDate;
    }
    
    public void setTimeSlotDate(Date timeSlotDate)
    {
        this.timeSlotDate=timeSlotDate;
    }
    
    public List<TimeSlotDate> loadTimeSlotDates(int appointmentDays, List<TimeSlot> timeSlotList, List <TimeSlotDate> timeSlotDateList)
    {
        for(int i=1;i<=appointmentDays;i++)
        {
            for(TimeSlot timeSlot : timeSlotList)
            {
                TimeSlotDate timeSlotDate = new TimeSlotDate();
                timeSlotDate.setTimeSlotDateID(timeSlotDateList);
                timeSlotDate.setTimeSlot(timeSlot);
                
                Date date = new Date();
                Calendar calendar = Calendar.getInstance(); 
                calendar.setTime(date); 
                calendar.add(Calendar.DATE, i);
                date = calendar.getTime();

                timeSlotDate.setTimeSlotDate(date);
                
                timeSlotDateList.add(timeSlotDate);
            }
        }
        
        return timeSlotDateList;
    }
    
    public void displayTimeSlotDateDetails(List<TimeSlotDate> timeSlotDateList)
    {   
        System.out.format ("%-10s%-15s%-15s%-15s%-20s",
                           "Count", "ID", "Date", "Start Time", "End Time");
        System.out.println();

        int count = 1;
        for(TimeSlotDate timeSlotDate : timeSlotDateList)
        {
            
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String date = sdf.format(timeSlotDate.getTimeSlotDate());
            System.out.format ("%-10s%-15s%-15s%-15s%-20s",
                                count, timeSlotDate.getTimeSlotDateID(),
                                date, timeSlotDate.getTimeSlot().getStartTime(), timeSlotDate.getTimeSlot().getEndTime());
            System.out.println();
            count++;
        }    
    }    
}
