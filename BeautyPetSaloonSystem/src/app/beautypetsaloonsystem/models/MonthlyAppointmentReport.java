package app.beautypetsaloonsystem.models;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class MonthlyAppointmentReport {
    private int monthNo;
    private int yearNo;
    private int attendedNo;
    private int missedNo;
    private int cancelledNo;
    private List<PetSitterAppointmentTaken> petSitterAppointmentTakenList = new ArrayList<PetSitterAppointmentTaken>();
    
    public MonthlyAppointmentReport()
    {
        this.setAttendedNo(0);
        this.setMissedNo(0);
        this.setCancelledNo(0);
    }
    
    public MonthlyAppointmentReport(int monthNo, int yearNo)
    {
        this.setMonthNo(monthNo);
        this.setYearNo(yearNo);
        this.setAttendedNo(0);
        this.setMissedNo(0);
        this.setCancelledNo(0);
        this.setPetSitterAppointmentTakenList(null);
    }
    
    public int getMonthNo(){
        return this.monthNo;
    }
    
    public void setMonthNo(int monthNo){
        this.monthNo=monthNo;
    }
    
    public int getYearNo(){
        return this.yearNo;
    }
    
    public void setYearNo(int yearNo)
    {
        this.yearNo=yearNo;
    }
    
    public int getAttendedNo(){
        return this.attendedNo;
    }
    
    public void setAttendedNo(int attendedNo)
    {
        this.attendedNo=attendedNo;
    }
    
    public int getMissedNo(){
        return this.missedNo;
    }
    
    public void setMissedNo(int missedNo){
        this.missedNo=missedNo;
    }
    
    public int getCancelledNo(){
        return this.cancelledNo;
    }
    
    public void setCancelledNo(int cancelledNo)
    {
        this.cancelledNo=cancelledNo;
    }
    
    public List getPetSitterAppointmentTakenList()
    {
        return this.petSitterAppointmentTakenList;
    }
    
    public void setPetSitterAppointmentTakenList(List<PetSitterAppointmentTaken> petSitterAppointmentTakenList)
    {
        this.petSitterAppointmentTakenList=petSitterAppointmentTakenList;
    }
    
    public void updatePetSitterInReport(boolean newReport, int monthNo, int yearNo, MonthlyAppointmentReport monthReport, List<Person> petSitterList)
    {
        if(monthReport.getMonthNo()==monthNo && monthReport.getYearNo()==yearNo)
        {
            if(newReport)
            {
                for(Person petSitter : petSitterList)
                {
                    PetSitter petSitter0 = (PetSitter)petSitter;
                    PetSitterAppointmentTaken petSitterAppointmentTaken = new PetSitterAppointmentTaken();
                    petSitterAppointmentTaken.setPetSitter((PetSitter)petSitter);
                    petSitterAppointmentTaken.setAppointmentTakenNo(0);
                    monthReport.getPetSitterAppointmentTakenList().add(petSitterAppointmentTaken);
                    monthReport.setPetSitterAppointmentTakenList(monthReport.getPetSitterAppointmentTakenList());
                    petSitter0.setPetSitterNotInReport(false);
                }
            }
            else
            {
                for(Person petSitter : petSitterList)
                {
                    PetSitter petSitter0 = (PetSitter)petSitter;
                    if(petSitter0.getPetSitterNotInReport())
                    {
                        PetSitterAppointmentTaken petSitterAppointmentTaken = new PetSitterAppointmentTaken();
                        petSitterAppointmentTaken.setPetSitter((PetSitter)petSitter);
                        petSitterAppointmentTaken.setAppointmentTakenNo(0);
                        monthReport.getPetSitterAppointmentTakenList().add(petSitterAppointmentTaken);
                        monthReport.setPetSitterAppointmentTakenList(monthReport.getPetSitterAppointmentTakenList());
                        petSitter0.setPetSitterNotInReport(false);
                    }
                }
            }
        }
    }
    
    public void createMonthlyReport(int monthNo, int yearNo, MonthlyAppointmentReport monthReport)
    {
        monthReport.setMonthNo(monthNo);
        monthReport.setYearNo(yearNo);
    }
    
    public void updateMonthlyReport(int monthNo, int yearNo, Appointment.AppointmentStatus oldAppointmentStatus, Appointment.AppointmentStatus newAppointmentStatus, MonthlyAppointmentReport monthReport, PetSitter petSitterInAppointment)
    {
        if(monthReport.getMonthNo()==monthNo && monthReport.getYearNo()==yearNo)
        {
            this.decrementAppointmentByStatus(monthReport, oldAppointmentStatus, petSitterInAppointment);
            this.incrementAppointmentByStatus(monthReport, newAppointmentStatus, petSitterInAppointment);
        }
    }   
        
    public void decrementAppointmentByStatus(MonthlyAppointmentReport monthReport, Appointment.AppointmentStatus oldAppointmentStatus, PetSitter petSitterInAppointment)
    {
        if(oldAppointmentStatus.equals(Appointment.AppointmentStatus.ATTENDED))
        {
            List<PetSitterAppointmentTaken> petSitterAppointmentTakenList = monthReport.getPetSitterAppointmentTakenList();
            for(PetSitterAppointmentTaken petSitterAppointmentTaken : petSitterAppointmentTakenList)
            {
                if(petSitterInAppointment.equals(petSitterAppointmentTaken.getPetSitter()))
                {
                    petSitterAppointmentTaken.setAppointmentTakenNo(petSitterAppointmentTaken.getAppointmentTakenNo()-1);
                }
            }
            monthReport.setAttendedNo(monthReport.getAttendedNo()-1);
        }
        
        if(oldAppointmentStatus.equals(Appointment.AppointmentStatus.UNATTENDED))
        {
            monthReport.setMissedNo(monthReport.getMissedNo()-1);
        }
        
        if(oldAppointmentStatus.equals(Appointment.AppointmentStatus.CANCELLED))
        {
            monthReport.setCancelledNo(monthReport.getCancelledNo()-1);
        }           
    }
    
    public void incrementAppointmentByStatus(MonthlyAppointmentReport monthReport, Appointment.AppointmentStatus newAppointmentStatus, PetSitter petSitterInAppointment)
    {
        if(newAppointmentStatus.equals(Appointment.AppointmentStatus.ATTENDED))
        {
            List<PetSitterAppointmentTaken> petSitterAppointmentTakenList = monthReport.getPetSitterAppointmentTakenList();
            for(PetSitterAppointmentTaken petSitterAppointmentTaken : petSitterAppointmentTakenList)
            {
                if(petSitterInAppointment.equals(petSitterAppointmentTaken.getPetSitter()))
                {
                    petSitterAppointmentTaken.setAppointmentTakenNo(petSitterAppointmentTaken.getAppointmentTakenNo()+1);
                }
            }
            monthReport.setAttendedNo(monthReport.getAttendedNo()+1);
        }
        
        if(newAppointmentStatus.equals(Appointment.AppointmentStatus.UNATTENDED))
        {
            monthReport.setMissedNo(monthReport.getMissedNo()+1);
        }
        
        if(newAppointmentStatus.equals(Appointment.AppointmentStatus.CANCELLED))
        {
            monthReport.setCancelledNo(monthReport.getCancelledNo()+1);
        }            
    }
    
    public void displayMonthlyAppointmentReport(List<MonthlyAppointmentReport> monthlyAppointmentReportList)
    {
        if(!monthlyAppointmentReportList.isEmpty())
        {
            for(MonthlyAppointmentReport monthReport : monthlyAppointmentReportList)
            {
                Calendar calendar = Calendar.getInstance();
                if(monthReport.getMonthNo()==calendar.get(Calendar.MONTH) && monthReport.getYearNo()==calendar.get(Calendar.YEAR))
                {
                    String monthName=new SimpleDateFormat("MMMM").format(calendar.getTime());
                    monthName=monthName + " " + calendar.get(Calendar.YEAR);
                    List<PetSitterAppointmentTaken> petSitterAppointmentTakenList = monthReport.getPetSitterAppointmentTakenList();
                    if(!petSitterAppointmentTakenList.isEmpty())
                    {
                        System.out.println("****************************************** PetSitter Appointment Taken Report ****************************"+monthName);
                        for(PetSitterAppointmentTaken petSitterAppointmentTaken : petSitterAppointmentTakenList)
                        {
                            System.out.println(petSitterAppointmentTaken.getPetSitter().getPersonName() + ": " + petSitterAppointmentTaken.getAppointmentTakenNo());
                        }
                        System.out.println("****************************************** PetSitter Appointment Taken Report *****************************************");
                        System.out.println();
                    }
                    System.out.println("************************************************* Appointment Report *************************************"+monthName);
                    System.out.println("Total Appointments Attended: " + monthReport.getAttendedNo());
                    System.out.println("Total Appointments Missed: " + monthReport.getMissedNo());
                    System.out.println("Total Appointments Cancelled: " + monthReport.getCancelledNo());
                    System.out.println("************************************************* Appointment Report **************************************************");
                    System.out.println();
                }
            }
        }
    }
}