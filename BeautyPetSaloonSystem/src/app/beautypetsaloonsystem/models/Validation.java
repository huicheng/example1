package app.beautypetsaloonsystem.models;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Kishani Joseph
 */
public class Validation {
    public static boolean isInputNull(Object input)
    {
        return input==null;
    }
    
    public static boolean isValidString(String input)
    {
        return input!=null && input.isEmpty()==false && input.matches("[a-zA-Z ]*");
    }
    
    public static boolean isValidNumber(String input)
    {
        return input!=null && input.isEmpty()== false && input.matches("[0-9]*");
    }
    
    public static boolean isIndexInList(int listSize, int userSelection)
    {
        return userSelection>=1 && userSelection<=listSize;
    }
    
    public static boolean isValidDOB(Date DOB)
    {
        Calendar minCalendar=Calendar.getInstance();
        minCalendar.add(Calendar.YEAR, -18);
        Date minDOB=minCalendar.getTime();
        
        Calendar maxCalendar=Calendar.getInstance();
        maxCalendar.add(Calendar.YEAR, -80);
        Date maxDOB=maxCalendar.getTime();
        
        return DOB.after(maxDOB) && DOB.before(minDOB);
    }
    
    public static boolean isValidContactNo(String input)
    {
        return input!=null && input.isEmpty()==false && input.length()>6 && input.matches("[-0-9]*");
    }
}
