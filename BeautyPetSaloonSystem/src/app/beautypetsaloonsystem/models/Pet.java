package app.beautypetsaloonsystem.models;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class Pet {
    private int petID;
    private String petName;
    private String petCategory;
    private String petBreed;
    private String petNote;
    private PetOwner petOwner;
    private boolean assignedToPetSitter;
    private List<PetTreatment> petTreatmentList=new ArrayList<PetTreatment>();
 
    public Pet(){}
    
    public Pet(List petList, String petName, String petCategory, String petBreed, PetOwner petOwner, boolean assignedToPetSitter)
    {
        this.setPetID(petList);
        this.setPetName(petName);
        this.setPetCategory(petCategory);
        this.setPetBreed(petBreed);
        this.setPetOwnerForPet(petOwner);
        this.setAssignedToPetSitter(assignedToPetSitter);
    }
    
    public Pet(List petList, String petName, String petCategory, String petBreed, boolean assignedToPetSitter)
    {
        this.setPetID(petList);
        this.setPetName(petName);
        this.setPetCategory(petCategory);
        this.setPetBreed(petBreed);
        this.setAssignedToPetSitter(assignedToPetSitter);
    }
    
    public int getPetID()
    {
        return this.petID;
    }
    
    public void setPetID(List<Pet> petList){
        int maxIDNum = 0;
        
        if(petList.isEmpty())
        {
            maxIDNum=1;
        }
        else
        {
            maxIDNum=petList.get(0).getPetID();
        
            for(Pet pet : petList)
            {
                if(pet.getPetID() > maxIDNum)
                {
                    maxIDNum = pet.getPetID();
                }
            }
            maxIDNum++;
        }
        
        this.petID=maxIDNum;
    }
    
    public String getPetName()
    {
        return this.petName;
    }
    
    public void setPetName(String petName)
    {
        if(Validation.isValidString(petName))
        {
            this.petName = petName;
        }
        else
        {
            try {
                throw new Exception();
            } catch (Exception ex) {
                System.out.println("Sorry! Pet Name can only contain letters.");
            }
        }
    }
    
    public String getPetCategory()
    {
        return this.petCategory;
    }
    
    public void setPetCategory(String petCategory)
    {
        if(Validation.isValidString(petCategory))
        {
            this.petCategory = petCategory;
        }
        else
        {
            try {
                throw new Exception();
            } catch (Exception ex) {
                System.out.println("Sorry! Pet Category can only contain letters.");
            }
        }
    }
    
    public String getPetBreed()
    {
        return this.petBreed;
    }
    
    public void setPetBreed(String petBreed)
    {
        if(Validation.isValidString(petBreed))
        {
            this.petBreed = petBreed;
        }
        else
        {
            try {
                throw new Exception();
            } catch (Exception ex) {
                System.out.println("Sorry! Pet Breed can only contain letters.");
            }
        }
    }
    
    public String getPetNote()
    {
        return this.petNote==null ? "None" : this.petNote;
    }
    
    public void setPetNote(String petNote)
    {
        if(Validation.isValidString(petNote))
        {
            this.petNote = petNote;
        }
        else
        {
            try {
                throw new Exception();
            } catch (Exception ex) {
                System.out.println("Sorry! Pet Note can only contain letters.");
            }
        }
    }
    
    public PetOwner getPetOwnerForPet()
    {
        return this.petOwner;        
    }
    
    public void setPetOwnerForPet(PetOwner petOwner)
    {
        this.petOwner=petOwner;
    }
    
    public boolean getAssignedToPetSitter()
    {
        return this.assignedToPetSitter;
    }
    
    public void setAssignedToPetSitter(boolean assignedToPetSitter)
    {
        this.assignedToPetSitter=assignedToPetSitter;
    }
    
    public String displayAssignedToPetSitter()
    {
        return this.getAssignedToPetSitter() ? "Yes" : "No";
    }
    
    public boolean hasPetUnassignedInList(List<Pet> petList)
    {
        for(Pet pet: petList)
        {
            if(!pet.getAssignedToPetSitter())
            {
                return true;
            }
        }
        return false;
    }
    
    public List getPetTreatmentsForPet()
    {
        return this.petTreatmentList;
    }
    
    public void setPetTreatmentsForPet(List<PetTreatment> petTreatmentList)
    {
        this.petTreatmentList=petTreatmentList;
    }
    
    public boolean isPetTreatmentDuplicate(PetTreatment petTreatmentSelected, Pet pet)
    {
        List<PetTreatment> petTreatmentList = pet.getPetTreatmentsForPet();
        for(PetTreatment petTreatment : petTreatmentList)
        {
            if(petTreatment.getPetTreatmentID()==petTreatmentSelected.getPetTreatmentID())
            {
                return true;
            }
        }
        
        return false;
    }
    
    public void displayPetDetails(List<Pet> petList)
    {
        if(!petList.isEmpty())
        {
            System.out.println("******************************************************* Pet List ******************************************************");        
            System.out.format("%-15s%-15s%-15s%-15s%-15s%-15s%-15s", "ID", "Pet Name", "Pet Category", "Pet Breed", "Pet Owner", "Assigned", "Pet Note");
            System.out.println();
            
            for(Pet pet : petList)
            {
                System.out.format("%-15s%-15s%-15s%-15s%-15s%-15s%-15s", pet.getPetID(), pet.getPetName(), pet.getPetCategory(), pet.getPetBreed(), pet.getPetOwnerForPet().getPersonName(), pet.displayAssignedToPetSitter(), pet.getPetNote());
                System.out.println();
            }            
            System.out.println("******************************************************* Pet List ******************************************************");
        }
    }
    
    public void displayPrescribedPetTreatmentDetails(List<Pet> petList)
    {
        if(!petList.isEmpty())
        {
            System.out.println("********************************************** Prescribed Treatment List **********************************************");        
            System.out.format("%-15s%-15s%-15s%-15s%-15s%-15s", "ID", "Pet Name", "Pet Category", "Pet Breed", "Pet Owner", "Pet Treatment");
            System.out.println();
            
            for(Pet pet : petList)
            {
                List<PetTreatment> petTreatmentsList = pet.getPetTreatmentsForPet();
                String prescribedTreatments="";
                if(petTreatmentsList.isEmpty())
                {
                    prescribedTreatments="None";
                }
                else
                {
                    for(PetTreatment petTreatment : petTreatmentsList)
                    {
                        prescribedTreatments+=petTreatment.getTreatmentName();
                        
                        List<PetProduct> petProductList=petTreatment.getPetProductsForTreatment();
                        
                        if(petProductList.isEmpty())
                        {
                            prescribedTreatments+="(No Product), ";
                        }
                        else
                        {
                            for(PetProduct petProduct : petProductList)
                            {
                                prescribedTreatments+="("+petProduct.getProductName()+ "), ";
                            }
                        }
                    }
                    prescribedTreatments=prescribedTreatments.replaceAll(", $", "");
                }
                
                System.out.format("%-15s%-15s%-15s%-15s%-15s%-15s", pet.getPetID(), pet.getPetName(), pet.getPetCategory(), pet.getPetBreed(), 
                                  pet.getPetOwnerForPet().getPersonName(), prescribedTreatments);
                System.out.println();
            }            
            System.out.println("********************************************** Prescribed Treatment List **********************************************");
        }
    }
    
    public void displayCurrentProductList(List<Pet> petList )
    {
        boolean displayNoTreatmentPrescribed=true;
        boolean displayNoProductInUse=true;
        
        if(!petList.isEmpty())
        {
            for(Pet pet : petList)
            {
                List<PetTreatment> petTreatmentsList = pet.getPetTreatmentsForPet();
                if(!petTreatmentsList.isEmpty())
                {
                    displayNoTreatmentPrescribed=false;
                    for(PetTreatment petTreatment : petTreatmentsList)
                    {
                        List<PetProduct> petProductList=petTreatment.getPetProductsForTreatment();
                        if(!petProductList.isEmpty())
                        {
                            displayNoProductInUse=false;
                            System.out.println("************************************************ Current Product List *************************************************");
                            System.out.format("%-20s%-20s%-20s%-20s", "ID", "Product Name", "Quantity In Stock", "Stock Status");
                            System.out.println();
            
                            for(PetProduct petProduct : petProductList)
                            {
                                System.out.format("%-20s%-20s%-20s%-20s",
                                        petProduct.getPetProductID(), petProduct.getProductName(),
                                        petProduct.getQuantityInStock(), petProduct.displayStockAvailability(petProduct.getStockAvailabilityStatus()));
                                System.out.println();
                            }
                            System.out.println("************************************************ Current Product List *************************************************"); 
                        }
                    }
                }
            }
            
            if(displayNoTreatmentPrescribed || displayNoProductInUse)
            {
                System.out.println("Sorry! At least one treatment using a product should be prescribed to a pet.");
            }
        }
    }
}