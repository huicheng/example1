package app.beautypetsaloonsystem.models;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Observable;

/**
 *
 * @author Kishani Joseph
 */
public class Appointment extends Observable{    
    private int appointmentID;
    private Pet pet;
    private PetSitter petSitter;
    private PetSitterTimeSlot petSitterTimeSlot;
    private String requestedService;
    private AppointmentStatus appointmentStatus=AppointmentStatus.AVAILABLE;
    public enum AppointmentStatus {AVAILABLE, BOOKED, CANCELLED, ATTENDED, UNATTENDED};
    
    public Appointment () {}
    
    public Appointment(List appointmentList, Pet pet, PetSitter petSitter, PetSitterTimeSlot petSitterTimeSlot, String requestedService, AppointmentStatus appointmentStatus)
    {
        this.setAppointmentID(appointmentList);
        this.setPet(pet);
        this.setPetSitter(petSitter);
        this.setPetSitterTimeSlot(petSitterTimeSlot);
        this.setRequestedService(requestedService);
        this.setAppointmentStatus(appointmentStatus);
    }
    
    public int getAppointmentID()
    {
        return this.appointmentID;
    }
    
    //calculates unique ID for appointment
    public void setAppointmentID(List<Appointment> appointmentList){
        int maxIDNum = 0;
        
        //if no appointments stored, maxIDNum will be 1
        if(appointmentList.isEmpty())
        {
            maxIDNum=1;
        }
        else
        {
            maxIDNum=appointmentList.get(0).getAppointmentID();
        
            for(Appointment appointment : appointmentList)
            {
                //if appointmentID found in list is greater than in maxIDNum
                if(appointment.getAppointmentID() > maxIDNum)
                {
                    //assign appointmentID to maxIDNum
                    maxIDNum = appointment.getAppointmentID();
                }
            }
            maxIDNum++;
        }
        
        this.appointmentID=maxIDNum;
    }
    
    public Pet getPet()
    {
        return this.pet;
    }
    
    public void setPet(Pet pet)
    {
        this.pet = pet;
    }    
    
    public PetSitter getPetSitter()
    {
        return this.petSitter;
    }
    
    public void setPetSitter(PetSitter petSitter)
    {
        this.petSitter = petSitter;
    }
    
    public PetSitterTimeSlot getPetSitterTimeSlot()
    {
        return this.petSitterTimeSlot;
    }
    
    public void setPetSitterTimeSlot(PetSitterTimeSlot petSitterTimeSlot)
    {
        this.petSitterTimeSlot=petSitterTimeSlot;
    }
    
    public AppointmentStatus getAppointmentStatus()
    {
        return this.appointmentStatus;
    }
    
    public void setAppointmentStatus(AppointmentStatus appointmentStatus)
    {
        if(this.appointmentStatus!=appointmentStatus)
        {            
            this.appointmentStatus=appointmentStatus;
            setChanged();
            
            if(this.appointmentStatus==AppointmentStatus.BOOKED || this.appointmentStatus==AppointmentStatus.ATTENDED)
            {
                notifyObservers(false);
            }
            else
            {
                notifyObservers(true);                
            }
        }
    }
    
    public String getRequestedService()
    {
        return this.requestedService;
    }
    
    public void setRequestedService(String requestedService)
    {
        this.requestedService=requestedService;
    }
    
    public void displayAppointmentDetails(List<Appointment> appointmentList)
    {   
        if(!appointmentList.isEmpty())
        {
            System.out.println("*************************************************** Appointment List ***************************************************");
            System.out.format("%-15s%-15s%-15s%-15s%-15s%-15s%-15s%-15s", "ID", "Pet Sitter", "Pet", "Service", "Date", "Start", "End", "Status");
            System.out.println();
            
            for(Appointment appointment : appointmentList)
            {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String appointmentDate = sdf.format(appointment.getPetSitterTimeSlot().getTimeSlotDate().getTimeSlotDate());
                System.out.format("%-15s%-15s%-15s%-15s%-15s%-15s%-15s%-15s",
                                appointment.getAppointmentID(),
                                appointment.getPetSitterTimeSlot().getPetSitter().getPersonName(),
                                appointment.getPet().getPetBreed(),
                                appointment.getRequestedService(),
                                appointmentDate,
                                appointment.getPetSitterTimeSlot().getTimeSlotDate().getTimeSlot().getStartTime(),                                
                                appointment.getPetSitterTimeSlot().getTimeSlotDate().getTimeSlot().getEndTime(),
                                appointment.getAppointmentStatus());
                System.out.println();
            }            
            System.out.println("*************************************************** Appointment List ***************************************************");
        }
    }
}