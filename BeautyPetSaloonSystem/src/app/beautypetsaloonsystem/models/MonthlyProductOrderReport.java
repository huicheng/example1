package app.beautypetsaloonsystem.models;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class MonthlyProductOrderReport {
    private int monthNo;
    private int yearNo;
    private int monthProductOrderTotal;
    private List<ProductOrderTotal> productOrderTotalList = new ArrayList<ProductOrderTotal>();
    
    public MonthlyProductOrderReport(){}
    
    public int getMonthNo()
    {
        return this.monthNo;
    }
    
    public void setMonthNo(int monthNo)
    {
        this.monthNo=monthNo;
    }
    
    public int getYearNo()
    {
        return this.yearNo;
    }
    
    public void setYearNo(int yearNo)
    {
        this.yearNo=yearNo;
    }
    
    public int getMonthProductOrderedTotal()
    {
        return this.monthProductOrderTotal;
    }
    
    public void setMonthProductOrderedTotal(int monthProductOrderTotal)
    {
        this.monthProductOrderTotal=monthProductOrderTotal;
    }
    
    public List getProductOrderTotalList()
    {
        return this.productOrderTotalList;
    }
    
    public void setProductOrderTotalList(List<ProductOrderTotal> productOrderTotalList)
    {
        this.productOrderTotalList=productOrderTotalList;        
    }
    
    public void updatePetProductInReport(boolean newReport, int monthNo, int yearNo, MonthlyProductOrderReport monthReport, List<PetProduct> petProductList)
    {
        if(monthReport.getMonthNo()==monthNo && monthReport.getYearNo()==yearNo)
        {
            if(newReport)
            {
                for(PetProduct petProduct : petProductList)
                {    
                    ProductOrderTotal productOrderTotal = new ProductOrderTotal ();
                    productOrderTotal.setPetProduct(petProduct);
                    productOrderTotal.setProductOrderTotal(0);
                    monthReport.getProductOrderTotalList().add(productOrderTotal);
                    monthReport.setProductOrderTotalList(monthReport.getProductOrderTotalList());
                    petProduct.setPetProductNotInReport(false);
                }
            }
            else
            {
                for(PetProduct petProduct : petProductList)
                {
                    if(petProduct.getPetProductNotInReport())
                    {
                        ProductOrderTotal productOrderTotal = new ProductOrderTotal ();
                        productOrderTotal.setPetProduct(petProduct);
                        productOrderTotal.setProductOrderTotal(0);
                        monthReport.getProductOrderTotalList().add(productOrderTotal);
                        monthReport.setProductOrderTotalList(monthReport.getProductOrderTotalList());
                        petProduct.setPetProductNotInReport(false);
                    }
                }
            }
        }
    }
    
    public void createMonthlyReport(int monthNo, int yearNo, MonthlyProductOrderReport monthReport)
    {
        monthReport.setMonthNo(monthNo);
        monthReport.setYearNo(yearNo);
    }
    
    public void updateMonthlyReport(int monthNo, int yearNo, MonthlyProductOrderReport monthReport, ProductOrder productOrder)
    {
        if(monthReport.getMonthNo()==monthNo && monthReport.getYearNo()==yearNo)
        {
            List<ProductOrderTotal> productOrderTotalList = monthReport.getProductOrderTotalList();
            for(ProductOrderTotal productOrderTotal : productOrderTotalList)
            {
                if(productOrder.getPetProductForOrder().equals(productOrderTotal.getPetProduct()))
                {
                    productOrderTotal.setProductOrderTotal(productOrderTotal.getProductOrderTotal()+Integer.parseInt(productOrder.getOrderedQuantity()));
                    
                }
                
            }
        }
    }
     
    public void displayMonthlyProductOrderReport(List<MonthlyProductOrderReport> monthlyProductOrderReportList)
    {
        if(!monthlyProductOrderReportList.isEmpty())
        {
            for(MonthlyProductOrderReport monthReport : monthlyProductOrderReportList)
            {
                Calendar calendar = Calendar.getInstance();
                if(monthReport.getMonthNo()==calendar.get(Calendar.MONTH) && monthReport.getYearNo()==calendar.get(Calendar.YEAR))
                {
                    String monthName=new SimpleDateFormat("MMMM").format(calendar.getTime());
                    monthName=monthName + " " + calendar.get(Calendar.YEAR);                    
                    List<ProductOrderTotal> productOrderTotalList = monthReport.getProductOrderTotalList();
                    if(!productOrderTotalList.isEmpty())
                    {
                        System.out.println("************************************************ Product Order Report ************************************"+monthName);
                        for(ProductOrderTotal productOrderTotal : productOrderTotalList)
                        {
                            System.out.println(productOrderTotal.getPetProduct().getProductName() + ": " + productOrderTotal.getProductOrderTotal());
                        }
                        System.out.println("************************************************ Product Order Report *************************************************");
                        System.out.println();
                    }
                }
            }
        }
    }
}
