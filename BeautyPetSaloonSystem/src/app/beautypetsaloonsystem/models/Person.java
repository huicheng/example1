package app.beautypetsaloonsystem.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class Person {
    private int personID;
    private String personName;
    private Date personDOB;
    private String personContactNo;
    
    public Person(){}
    
    public Person(List personList, String personName, String personDOB, String personContactNo)
    {
        this.setPersonID(personList);
        this.setPersonName(personName);
        this.setPersonDOB(personDOB);
        this.setPersonContactNo(personContactNo);
    }
    
    public int getPersonID()
    {
        return this.personID;
    }
    
    public void setPersonID(List<Person> personList){
        
        this.personID=retrievePersonID(personList);
    }
    
    public int retrievePersonID(List<Person> personList)
    {
        int maxIDNum = 0;
        
        //if no persons stored, maxID will be 1        
        if(personList.isEmpty())
        {
            maxIDNum=1;
        }
        else
        {
            maxIDNum=personList.get(0).getPersonID();
        
            for(Person person : personList)
            {
                //if personID found in list is greater than in maxIDNum
                if(person.getPersonID() > maxIDNum)
                {
                    //assign personID to maxIDNum
                    maxIDNum = person.getPersonID();
                }
            }
            maxIDNum++;
        }
        
        return maxIDNum;
    }
    
    public String getPersonName()
    {
        return this.personName;
    }
    
    public void setPersonName(String personName)
    {
        if(Validation.isValidString(personName))
        {
            this.personName = personName;
        }
        else
        {
            try {
                throw new Exception();
            } catch (Exception ex) {
                System.out.println("Sorry! Name can only contain letters.");
            }
        }
    }
    
    public Date getPersonDOB()
    {
        return this.personDOB;
    }
    
    public void setPersonDOB(String personDOB)
    {
        try {
            Date DOB=new SimpleDateFormat("dd/MM/yyyy").parse(personDOB);
            if(Validation.isValidDOB(DOB))
            {
                this.personDOB=DOB;
            }
            else
            {
                System.out.println("Sorry! Date of Birth should be 18 to 80 years before today.");
            }
        } catch (Exception ex) {
            System.out.println("Sorry! Date of Birth has to be in the format of dd/mmy/yyyy");
        }      
    }
    
    public String getPersonContactNo()
    {
        return this.personContactNo;
    }
    
    public void setPersonContactNo(String personContactNo)
    {
        if(Validation.isValidContactNo(personContactNo))
        {
            this.personContactNo = personContactNo;
        }
        else
        {
            try {
                throw new Exception();
            } catch (Exception ex) {
                System.out.println("Sorry! Contact No must be at least seven (7) digits containing only numbers and/or a hyphen.");
            }
        }
    }
    
    public void displayPersonDetails(List<Person> personList, String listTitle)
    {           
        System.out.println("*************************************************** "+listTitle+" ***************************************************");
        System.out.format("%-20s%-20s%-20s%-20s", "ID", "Name", "Birth Date", "Contact No");
        System.out.println();
        
        for(Person person : personList)
        {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String dateOfBirth = sdf.format(person.getPersonDOB());
            System.out.format("%-20s%-20s%-20s%-20s", person.getPersonID(), person.getPersonName(), dateOfBirth, person.getPersonContactNo());
            System.out.println();
        }                
        System.out.println("*************************************************** "+listTitle+" ***************************************************");
    }
}
