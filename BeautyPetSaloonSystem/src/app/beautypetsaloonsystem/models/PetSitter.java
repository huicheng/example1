package app.beautypetsaloonsystem.models;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class PetSitter extends Employee{
    private boolean petSitterNotInReport;
    private List<Pet> petAssignedList = new ArrayList<Pet>();
    
    public PetSitter(){}
    
    public PetSitter(List petSitterList, String personName, String personDOB, String personContactNo, String designationLevel)
    {
        this.setPersonID(petSitterList);
        this.setPersonName(personName);
        this.setPersonDOB(personDOB);
        this.setPersonContactNo(personContactNo);
        this.setDesignationLevel(designationLevel);
        this.setPetSitterNotInReport(true);
    }
        
    public boolean getPetSitterNotInReport()
    {
        return this.petSitterNotInReport;
    }
    
    public void setPetSitterNotInReport(boolean petSitterNotInReport)
    {
        this.petSitterNotInReport=petSitterNotInReport;
    }
    
    public List getPetAssignedList()
    {
        return this.petAssignedList;
    }
    
    public void setPetAssignedList(Pet pet)
    {
        this.petAssignedList.add(pet);
    }
    
    public void displayPetAssignedDetails(List<Person> petSitterList)
    {   
        System.out.println("************************************************** Pet Assigned List **************************************************");
        System.out.format ("%-20s%-20s%-20s%-20s", "Pet Sitter", "Pet Category", "Pet Breed", "Pet Owner");
        System.out.println();
        
        for(Person person: petSitterList)
        {
            PetSitter petSitter=(PetSitter)person;
            List<Pet> petList=petSitter.getPetAssignedList();
            if(!petList.isEmpty())
            {
                for(Pet pet : petList)
                {
                    System.out.format ("%-20s%-20s%-20s%-20s",
                                   petSitter.getPersonName(), pet.getPetCategory(),
                                   pet.getPetBreed(), pet.getPetOwnerForPet().getPersonName());
                    System.out.println();
                }
            }
        }
        System.out.println("************************************************** Pet Assigned List **************************************************");
    }
}
