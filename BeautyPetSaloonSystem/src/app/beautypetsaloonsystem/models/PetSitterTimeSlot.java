package app.beautypetsaloonsystem.models;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Kishani Joseph
 */
public class PetSitterTimeSlot implements Observer{
    private int petSitterTimeSlotID;
    private PetSitter petSitter;
    private TimeSlotDate timeSlotDate;
    private boolean slotAvailibilityStatus;
    
    public PetSitterTimeSlot(){}
    
    public PetSitterTimeSlot(PetSitter petSitter, TimeSlotDate timeSlotDate){
        this.setPetSitter(petSitter);
        this.setTimeSlotDate(timeSlotDate);
    }
    
    public int getPetSitterTimeSlotID()
    {
        return this.petSitterTimeSlotID;
    }
    
    public void setPetSitterTimeSlotID(List<PetSitterTimeSlot> petSitterTimeSlotList){
        int maxIDNum = 0;
        
        if(petSitterTimeSlotList.isEmpty())
        {
            maxIDNum=1;
        }
        else
        {
            maxIDNum=petSitterTimeSlotList.get(0).getPetSitterTimeSlotID();
        
            for(PetSitterTimeSlot petSitterTimeSlot : petSitterTimeSlotList)
            {
                if(petSitterTimeSlot.getPetSitterTimeSlotID() > maxIDNum)
                {
                    maxIDNum = petSitterTimeSlot.getPetSitterTimeSlotID();
                }
            }
            maxIDNum++;
        }
        
        this.petSitterTimeSlotID=maxIDNum;
    }
    
    public PetSitter getPetSitter()
    {
        return this.petSitter;
    }
    
    public void setPetSitter(PetSitter petSitter)
    {
        this.petSitter=petSitter;
    }
    
    public TimeSlotDate getTimeSlotDate()
    {
        return this.timeSlotDate;
    }
    
    public void setTimeSlotDate(TimeSlotDate timeSlotDate)
    {
        this.timeSlotDate=timeSlotDate;
    }
    
    public boolean getSlotAvailabilityStatus()
    {
        return this.slotAvailibilityStatus;
    }
    
    public void setSlotAvailabilityStatus(boolean slotAvailabilityStatus)
    {
        this.slotAvailibilityStatus=slotAvailabilityStatus;
    }
    
    public String displaySlotAvailability(boolean slotAvailabilityStatus)
    {
        return slotAvailabilityStatus ? "AVAILABLE" : "BOOKED";
    }
    
    public List<PetSitterTimeSlot> getPetSitterTimeSlotsForPetSitter(PetSitter petSitter, List<PetSitterTimeSlot> petSitterTimeSlotList)
    {
        List<PetSitterTimeSlot> timeSlotListForPetSitter = new ArrayList<PetSitterTimeSlot>();
        
        for(PetSitterTimeSlot petSitterTimeSlot : petSitterTimeSlotList)
        {
            if(petSitter.getPersonID()==petSitterTimeSlot.getPetSitter().getPersonID())
            {
                timeSlotListForPetSitter.add(petSitterTimeSlot);
            }
        }
        
        return timeSlotListForPetSitter;
    }
    
    public List<PetSitterTimeSlot> loadPetSitterTimeSlots(PetSitter petSitter, List<TimeSlotDate> timeSlotDateList, List<PetSitterTimeSlot> petSitterTimeSlotList)
    {
        List<PetSitterTimeSlot> timeSlotListForPetSitter=this.getPetSitterTimeSlotsForPetSitter(petSitter, petSitterTimeSlotList);
        
        for(TimeSlotDate timeSlotDate : timeSlotDateList)
        {
            boolean hasNotTimeSlotDate = true;
            
            for(PetSitterTimeSlot timeSlotForPetSitter : timeSlotListForPetSitter)
            {
                if(timeSlotDate.getTimeSlotDateID()==timeSlotForPetSitter.getTimeSlotDate().getTimeSlotDateID())
                {
                    hasNotTimeSlotDate=false;
                    break;               
                }
            }

            if(hasNotTimeSlotDate)
            {
                PetSitterTimeSlot petSitterTimeSlot = new PetSitterTimeSlot();
                petSitterTimeSlot.setPetSitterTimeSlotID(petSitterTimeSlotList);
                petSitterTimeSlot.setPetSitter(petSitter);
                petSitterTimeSlot.setTimeSlotDate(timeSlotDate);
                petSitterTimeSlot.setSlotAvailabilityStatus(true);
                petSitterTimeSlotList.add(petSitterTimeSlot);
            }            
        }
        
        return petSitterTimeSlotList;
            
    }
    
    public void displayPetSitterTimeSlotDateDetails(List<PetSitterTimeSlot> petSitterTimeSlotList)
    {
        System.out.println("********************************************* Pet Sitter Availability List *********************************************");
        System.out.format("%-20s%-20s%-20s%-20s%-20s%-20s",
                           "Option", "Pet Sitter", "Date", "Start Time", "End Time", "Slot Status");
        System.out.println();

        int count = 1;
        for(PetSitterTimeSlot PetSitterTimeSlot : petSitterTimeSlotList)
        {
            
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String date = sdf.format(PetSitterTimeSlot.getTimeSlotDate().getTimeSlotDate());
            System.out.format("%-20s%-20s%-20s%-20s%-20s%-20s",
                                count, PetSitterTimeSlot.getPetSitter().getPersonName(), date,
                                PetSitterTimeSlot.getTimeSlotDate().getTimeSlot().getStartTime(),
                                PetSitterTimeSlot.getTimeSlotDate().getTimeSlot().getEndTime(),
                                PetSitterTimeSlot.displaySlotAvailability(PetSitterTimeSlot.getSlotAvailabilityStatus()));
            System.out.println();
            count++;
        }
        System.out.println("********************************************* Pet Sitter Availability List *********************************************");
    }
    
    @Override
    public void update(Observable obs, Object arg) {
        this.setSlotAvailabilityStatus((Boolean)arg);
    }
}