package app.beautypetsaloonsystem.models;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class PetTreatment {
    private int petTreatmentID;
    private String treatmentName;
    private List<PetProduct> petProductList = new ArrayList<PetProduct>();
    
    public PetTreatment(){}
    
    public PetTreatment(List petTreatmentList, String treatmentName)
    {
        this.setPetTreatmentID(petTreatmentList);
        this.setTreatmentName(treatmentName);
    }
    
    public int getPetTreatmentID()
    {
        return this.petTreatmentID;
    }
    
    public void setPetTreatmentID(List<PetTreatment> petTreatmentList){
        int maxIDNum = 0;
        
        if(petTreatmentList.isEmpty())
        {
            maxIDNum=1;
        }
        else
        {
            maxIDNum=petTreatmentList.get(0).getPetTreatmentID();
        
            for(PetTreatment petTreatment : petTreatmentList)
            {
                if(petTreatment.getPetTreatmentID() > maxIDNum)
                {
                    maxIDNum = petTreatment.getPetTreatmentID();
                }
            }
            maxIDNum++;
        }
        
        this.petTreatmentID=maxIDNum;
    }
    
    public String getTreatmentName()
    {
        return this.treatmentName;
    }
    
    public void setTreatmentName(String treatmentName)
    {
        if(Validation.isValidString(treatmentName))
        {
            this.treatmentName = treatmentName;
        }
        else
        {
            try {
                throw new Exception();
            } catch (Exception ex) {
                System.out.println("Sorry! Treatment Name can only contain letters.");
            }
        }
    }
    
    public List getPetProductsForTreatment()
    {
        return this.petProductList;
    }
    
    public void setPetProductsForTreatment(List<PetProduct>  petProductList)
    {
        this.petProductList=petProductList;
    }
    
    public boolean isProductDuplicate(PetProduct petProductSelected, PetTreatment petTreatment)
    {
        List<PetProduct> petProductList = petTreatment.getPetProductsForTreatment();
        for(PetProduct petProduct : petProductList)
        {
            if(petProduct.getPetProductID()==petProductSelected.getPetProductID())
            {
                return true;
            }
        }
        
        return false;
    }
    
    public void displayPetTreatmentDetails(List<PetTreatment> petTreatmentList)
    {
        if(!petTreatmentList.isEmpty())
        {
            System.out.println("************************************************** Pet Treatment List **************************************************");
            System.out.format("%-20s%-40s%-40s", "ID", "Treatment Name", "Products Used");
            System.out.println();
            
            for(PetTreatment petTreatment : petTreatmentList)
            {
                List<PetProduct> petProductList = petTreatment.getPetProductsForTreatment();
                String petProducts="";
                if(petProductList.isEmpty())
                {
                    petProducts="None";
                }
                else
                {
                    for(PetProduct petProduct : petProductList)
                    {
                        petProducts+=petProduct.getProductName()+ ", ";
                    }
                    petProducts=petProducts.replaceAll(", $", "");
                }
                
                System.out.format("%-20s%-40s%-40s", petTreatment.getPetTreatmentID(), petTreatment.getTreatmentName(), petProducts);
                System.out.println();
            }            
            System.out.println("************************************************** Pet Treatment List **************************************************");
        }
    }
}