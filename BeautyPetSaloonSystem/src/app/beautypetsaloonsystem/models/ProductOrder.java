package app.beautypetsaloonsystem.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class ProductOrder {
    private int productOrderID;
    private PetProduct petProduct;
    private String orderedQuantity;
    private Date orderedODate;
    
    public ProductOrder(){}
    
     public int getProductOrderID()
    {
        return this.productOrderID;
    }
    
    public void setProductOrderID(List<ProductOrder> productOrderList){
        int maxIDNum = 0;
        
        if(productOrderList.isEmpty())
        {
            maxIDNum=1;
        }
        else
        {
            maxIDNum=productOrderList.get(0).getProductOrderID();
        
            for(ProductOrder productOrder : productOrderList)
            {
                if(productOrder.getProductOrderID() > maxIDNum)
                {
                    maxIDNum = productOrder.getProductOrderID();
                }
            }
            maxIDNum++;
        }
        
        this.productOrderID=maxIDNum;
    }
    
    public PetProduct getPetProductForOrder()
    {
        return this.petProduct;
    }
    
    public void setPetProductForOrder(PetProduct petProduct)
    {
        this.petProduct=petProduct;
    }
    
    public String getOrderedQuantity()
    {
        return this.orderedQuantity;
    }
    
    public void setOrderedQuantity(String orderQuantity)
    {        
        if(Validation.isValidNumber(orderQuantity))
        {
            this.orderedQuantity = orderQuantity;
        }
        else
        {
            try {
                throw new Exception();
            } catch (Exception ex) {
                System.out.println("Sorry! Quantity In-Stock can only contain numbers.");
            }
        }
        
    }
    
    public Date getOrderedDate()
    {
        return this.orderedODate;
    }
    
    public void setOrderedDate()
    {
        this.orderedODate = new Date();
    }
    
    public void displayProductOrderDetails(List<ProductOrder> productOrderList)
    { 
        if(!productOrderList.isEmpty())
        {
            System.out.println("************************************************** Product Order List *************************************************");
            System.out.format("%-20s%-20s%-20s%-20s", "ID", "Product Name", "Order Quantity", "Ordered Date");
            System.out.println();
            
            for(ProductOrder productOrder : productOrderList)
            {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String orderedDate = sdf.format(productOrder.getOrderedDate());
            
                System.out.format("%-20s%-20s%-20s%-20s",
                                    productOrder.getProductOrderID(),
                                    productOrder.getPetProductForOrder().getProductName(),
                                    productOrder.getOrderedQuantity(), orderedDate);
                System.out.println();
            }            
            System.out.println("************************************************** Product Order List *************************************************");
        }
    }
}