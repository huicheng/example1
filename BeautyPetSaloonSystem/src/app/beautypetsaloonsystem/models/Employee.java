package app.beautypetsaloonsystem.models;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class Employee extends Person{
    private String designationLevel;
    
    public Employee() {}
    
    public String getDesignationLevel()
    {
        return this.designationLevel;
    }
    
    public void setDesignationLevel(String designationLevel)
    {
        this.designationLevel=designationLevel;
    }
    
    public void displayEmployeeDetails(List<Person> personList, String listTitle)
    {           
        System.out.println("*************************************************** "+listTitle+" ***************************************************");
        System.out.format("%-20s%-20s%-20s%-20s%-20s", "ID", "Name", "Birth Date", "Contact No", "Designation");
        System.out.println();
        
        for(Person person : personList)
        {
            Employee employee=(Employee)person;
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String dateOfBirth = sdf.format(employee.getPersonDOB());
            System.out.format("%-20s%-20s%-20s%-20s%-20s", employee.getPersonID(), employee.getPersonName(), dateOfBirth, employee.getPersonContactNo(), employee.getDesignationLevel());
            System.out.println();
        }                
        System.out.println("*************************************************** "+listTitle+" ***************************************************");
    }
}
