package app.beautypetsaloonsystem.models;

import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class PetOwner extends Person{
    public PetOwner(){}
    
    public PetOwner(List petOwnerList, String personName, String personDOB, String personContactNo)
    {
        this.setPersonID(petOwnerList);
        this.setPersonName(personName);
        this.setPersonDOB(personDOB);
        this.setPersonContactNo(personContactNo);
    }
}
