package app.beautypetsaloonsystem.models;

/**
 *
 * @author Kishani Joseph
 */
public class ProductOrderTotal {
    private PetProduct petProduct;
    private int productOrderTotal;

    public ProductOrderTotal(){}
    
    public PetProduct getPetProduct()
    {
        return this.petProduct;
    }
    
    public void setPetProduct(PetProduct petProduct)
    {
        this.petProduct=petProduct;
    }
    
    public int getProductOrderTotal()
    {
        return this.productOrderTotal;
    }
    
    public void setProductOrderTotal(int productOrderTotal)
    {
        this.productOrderTotal=productOrderTotal;
    }
}
