package app.beautypetsaloonsystem.models;

import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class TimeSlot {
    private int timeID;
    private int startTime;
    private int endTime;
    
    public TimeSlot(){}
    
    public TimeSlot(int startTime, int endTime)
    {
        this.setStartTime(startTime);
        this.setEndTime(endTime);
    }
    
    public int getTimeID()
    {
        return this.timeID;
    }
    
    public void setTimeID(List<TimeSlot> timeSlotList){
        int maxIDNum = 0;
        
        if(timeSlotList.isEmpty())
        {
            maxIDNum=1;
        }
        else
        {
            maxIDNum=timeSlotList.get(0).getTimeID();
        
            for(TimeSlot timeSlot : timeSlotList)
            {
                if(timeSlot.getTimeID() > maxIDNum)
                {
                    maxIDNum = timeSlot.getTimeID();
                }
            }
            maxIDNum++;
        }
        
        this.timeID=maxIDNum;
    }
    
    public int getStartTime()
    {
        return this.startTime;
    }
    
    public void setStartTime(int startTime)
    {
        this.startTime=startTime;
    }
    
    public int getEndTime()
    {
        return this.endTime;
    }
    
    public void setEndTime(int endTime)
    {
        this.endTime=endTime;
    }
    
    public List<TimeSlot> loadTimeSlots(int shiftStart, int shiftDuration,  int slotDuration, List <TimeSlot> timeSlotList)
    {
        int slot=shiftStart;
        
        for(int i=0;i<shiftDuration;i++)
        {
            TimeSlot timeSlot = new TimeSlot();
            timeSlot.setTimeID(timeSlotList);
            timeSlot.setStartTime(slot+i);
            timeSlot.setEndTime(timeSlot.getStartTime()+slotDuration);            
            
            timeSlotList.add(timeSlot);
        }
        
        return timeSlotList;
    }
    
    public void displayTimeSlotDetails(List<TimeSlot> timeSlotList)
    {   
        System.out.format ("%-10s%-15s%-15s%-20s",
                           "Count", "ID", "Start Time", "End Time");
        System.out.println();

        int count = 1;
        for(TimeSlot timeSlot : timeSlotList)
        {
            System.out.format ("%-10s%-15d%-15d%-20s",
                                count, timeSlot.getTimeID(),
                                timeSlot.getStartTime(), timeSlot.getEndTime());
            System.out.println();
            count++;
        }    
    }
}
