package app.beautypetsaloonsystem;

import app.beautypetsaloonsystem.controllers.AppointmentController;
import app.beautypetsaloonsystem.controllers.AssignPetController;
import app.beautypetsaloonsystem.controllers.CommonController;
import app.beautypetsaloonsystem.controllers.MonthlySaloonReportController;
import app.beautypetsaloonsystem.controllers.PetController;
import app.beautypetsaloonsystem.controllers.PetOwnerController;
import app.beautypetsaloonsystem.controllers.PetProductController;
import app.beautypetsaloonsystem.controllers.PetSitterController;
import app.beautypetsaloonsystem.controllers.PetTreatmentController;
import app.beautypetsaloonsystem.controllers.PrescribePetTreatmentController;
import app.beautypetsaloonsystem.controllers.ProductOrderController;
import app.beautypetsaloonsystem.controllers.TimeSlotController;
import app.beautypetsaloonsystem.controllers.TimeSlotDateController;

/**
 *
 * @author Kishani Joseph
 */
public class Main {
    public static void main(String[] args)
    {
        loadPreRegisteredData();
        updateTimeSlots();
        displayHomePage();
    }
    
    public static void loadPreRegisteredData()
    {
        CommonController commonController=new CommonController();
        commonController.loadPreRegisteredData();
    }
    
    public static void updateTimeSlots()
    {
        TimeSlotController timeSlotController = new TimeSlotController();        
        timeSlotController.updateTimeSlots();
        
        TimeSlotDateController timeSlotDateController = new TimeSlotDateController();        
        timeSlotDateController.updateTimeSlotDates();
    }
    
    public static void displayHomePage()
    {
       CommonController commonController = new CommonController();
        
        if(commonController.showHomePage())
        {
            showHomePageSelection(commonController.readUserSelection());            
        }
    }
    
    public static void displayManagePetTreatmentPage()
    {
        CommonController commonController = new CommonController();
        PetTreatmentController petTreatmentController = new PetTreatmentController();
        
        if(petTreatmentController.showManagePetTreatmentPage())
        {
            showPetTreatmentSelection(commonController.readUserSelection());
        }       
    }
    
    public static void displayManageAppointmentPage()
    {
        CommonController commonController = new CommonController();
        AppointmentController appointmentController = new AppointmentController();
        
        if(appointmentController.showManageAppointmentPage())
        {
            showAppointmentSelection(commonController.readUserSelection());
        }       
    }
    
    public static void displayAssignPetPage()
    {
        CommonController commonController = new CommonController();     
        boolean continueAssigning = true;
        while(continueAssigning)
        {
            AssignPetController assignPetController = new AssignPetController();
        
            if(assignPetController.showAssignPetPage())
            {
                assignPetController.assignPet();
                
                commonController.showContinuePage("Would you like to assign another pet to a pet sitter? ");
                continueAssigning=commonController.readContinueSelection();
            }
        }
    }
    
    public static void displayRegistrationPage()
    {
        CommonController commonController = new CommonController();
        if(commonController.showRegistrationPage())
        {
            showRegistrationSelection(commonController.readUserSelection());
        }
    }
    
    public static void displayProductOrderPage()
    {
        CommonController commonController = new CommonController();        
        boolean continueOrdering = true;
        while(continueOrdering)
        {
            ProductOrderController productOrderController = new ProductOrderController();
        
            if(productOrderController.showProductOrderPage())
            {
                productOrderController.saveProductOrderDetails();
                productOrderController.viewProductOrderList();
                
                commonController.showContinuePage("Would you like to order another product? ");
                continueOrdering=commonController.readContinueSelection();
            }
        }
    }
    
    public static void displayModifyPetNotePage()
    {
        CommonController commonController = new CommonController();
        boolean continueModifying = true;
        while(continueModifying)
        {
            PetController petController = new PetController();
            if(petController.showModifyPetNotePage());
            {
                petController.modifyPetNote();
                
                commonController.showContinuePage("Would you like to modify another pet note? ");
                continueModifying=commonController.readContinueSelection();
            }
        }
    }
    
    public static void displayCurrentProductListPage()
    {
        CommonController commonController = new CommonController();
        boolean continueModifying = true;
        while(continueModifying)
        {
            PetController petController = new PetController();
            if(petController.showCurrentProductListPage())
            {
                petController.viewCurrentProductList();
                
                commonController.showContinuePage("Would you like to reload the list? ");
                continueModifying=commonController.readContinueSelection();
            }
        }
    }
    
    public static void displayMonthlySaloonReport()
    {
        CommonController commonController = new CommonController();
        boolean continueViewing = true;
        while(continueViewing)
        {
            MonthlySaloonReportController monthlySaloonReportController = new MonthlySaloonReportController();
                
            if(monthlySaloonReportController.showMonthlySaloonReportPage())
            {
                monthlySaloonReportController.showMonthlySaloonReport();
                
                commonController.showContinuePage("Would you like to reload report? ");
                continueViewing=commonController.readContinueSelection();
            }
        }
    }
    
    public static void displayRegisterPetSitterPage()
    {
        CommonController commonController = new CommonController();
        boolean continueRegistration = true;
        while(continueRegistration)
        {
            PetSitterController petSitterController = new PetSitterController();
            if(petSitterController.showRegisterPetSitterPage())
            {
                petSitterController.registerPetSitterDetails();
                petSitterController.viewPetSitterList();
                
                commonController.showContinuePage("Would you like to register another pet sitter? ");
                continueRegistration=commonController.readContinueSelection();
            }
        }
    }
    
    public static void displayRegisterPetOwnerPage()
    {
        CommonController commonController = new CommonController();
        boolean continueRegistration = true;
        while(continueRegistration)
        {
            PetOwnerController petOwnerController = new PetOwnerController();
            if(petOwnerController.showRegisterPetOwnerPage())
            {
                petOwnerController.registerPetOwnerDetails();
                petOwnerController.viewPetOwnerList();
                
                commonController.showContinuePage("Would you like to register another pet owner? ");
                continueRegistration=commonController.readContinueSelection();
            }
        }
    }
    
    public static void displayRegisterPetPage()
    {
        CommonController commonController = new CommonController();
        boolean continueRegistration = true;
        while(continueRegistration)
        {
            PetController petController = new PetController();
            if(petController.showRegisterPetPage())
            {
                petController.registerPetDetails();
                petController.viewPetList();
                
                commonController.showContinuePage("Would you like to register another pet? ");
                continueRegistration=commonController.readContinueSelection();
            }
        }
    }
    
    public static void displayRegisterPetProductPage()
    {
        CommonController commonController = new CommonController();
        boolean continueRegistration = true;
        while(continueRegistration)
        {
            PetProductController petProductController = new PetProductController();
            if(petProductController.showRegisterPetProductPage())
            {
                petProductController.registerPetProductDetails();
                petProductController.viewPetProductList();
                
                commonController.showContinuePage("Would you like to register another pet product? ");
                continueRegistration=commonController.readContinueSelection();
            }
        }
    }
    
    public static void displayRegisterPetTreatmentPage()
    {
        CommonController commonController = new CommonController();
        boolean continueRegistration = true;
        while(continueRegistration)
        {
            PetTreatmentController petTreatmentController = new PetTreatmentController();
            if(petTreatmentController.showRegisterPetTreatmentPage())
            {
                petTreatmentController.registerPetTreatmentDetails();
                petTreatmentController.viewPetTreatmentList();
                
                commonController.showContinuePage("Would you like to register another pet treatment? ");
                continueRegistration=commonController.readContinueSelection();
            }
        }
    }
    
    public static void displayBookAppointmentPage()
    {
        CommonController commonController = new CommonController();
        boolean continueBooking = true;
        while(continueBooking)
        {
            AppointmentController appointmentController = new AppointmentController();
            if(appointmentController.showBookAppointmentPage())
            {
                appointmentController.saveAppointmentDetails();
                appointmentController.viewAppointmentList();
                
                commonController.showContinuePage("Would you like to book another appointment? ");
                continueBooking=commonController.readContinueSelection();
            }
        }
    }
    
    public static void displayUpdateAppointmentPage()
    {
        CommonController commonController = new CommonController();
        boolean continueUpdating = true;
        while(continueUpdating)
        {
            AppointmentController appointmentController = new AppointmentController();
            if(appointmentController.showUpdateAppointmentPage())
            {
                appointmentController.updateAppointmentDetails();
                appointmentController.viewAppointmentList();
                
                commonController.showContinuePage("Would you like to update another appointment? ");
                continueUpdating=commonController.readContinueSelection();
            }
        }
    }    
    
    public static void displayAddPetProductForTreatmentPage()
    {
        CommonController commonController = new CommonController();
        boolean continueAdding = true;
        while(continueAdding)
        {
            PetTreatmentController petTreatmentController = new PetTreatmentController();
            if(petTreatmentController.showAddPetProductsForTreatmentPage())
            {
                petTreatmentController.addPetProductsForTreatment();
                
                commonController.showContinuePage("Would you like to add another product to pet treatment? ");
                continueAdding=commonController.readContinueSelection();
            }
        }      
    }
    
    public static void displayPrescribePetTreatmentPage()
    {
        CommonController commonController = new CommonController();
        boolean continueAdding = true;
        while(continueAdding)
        {
            PrescribePetTreatmentController prescribePetTreatmentController = new PrescribePetTreatmentController();
            if(prescribePetTreatmentController.showPrescribePetTreatmentPage());
            {
                prescribePetTreatmentController.prescribePetTreatments();
                
                commonController.showContinuePage("Would you like to prescribe another treatment to a pet? ");
                continueAdding=commonController.readContinueSelection();
            }
        }
    }
    
    public static void showHomePageSelection(int userSelection)
    {
        switch(userSelection)
        {
            case 1:
            {                
                displayManageAppointmentPage();
                displayHomePage();
                break;
            }
            case 2:
            {
                displayAssignPetPage();
                displayHomePage();
                break;
            }
            case 3:
            {
                displayRegistrationPage();
                displayHomePage();
                break;
                
            }
            case 4:
            {
                displayManagePetTreatmentPage();
                displayHomePage();
                break;
            }
            case 5:
            {
                displayProductOrderPage();
                displayHomePage();
                break;
            }
            case 6:
            {
                displayModifyPetNotePage();
                displayHomePage();
                break;
            }
            case 7:
            {
                displayCurrentProductListPage();
                displayHomePage();
                break;                
            }
            case 8:
            {
                displayMonthlySaloonReport();
                displayHomePage();
                break;
            }
            case 99:
            {
                System.exit(0);
                break;
            }
            default:
            {
                displayHomePage();
                break;                
            }
        }
    }
    
    public static void showRegistrationSelection(int userSelection)
    {
        switch(userSelection)
        {
            case 1:
            {
                displayRegisterPetSitterPage();
                displayRegistrationPage();
                break;                
            }
            case 2:
            {                
                displayRegisterPetOwnerPage();            
                displayRegistrationPage();
                break;
            }
            case 3:
            {
                displayRegisterPetPage();
                displayRegistrationPage();
                break;
            }             
            case 4:
            {                
                displayRegisterPetProductPage();
                displayRegistrationPage();
                break;
            }
            case 5:
            {
                displayRegisterPetTreatmentPage();
                displayRegistrationPage();
                break;                
            }
            case 98:
            {
                displayHomePage();
                break;
            }            
            case 99:
            {
                System.exit(0);
                break;
            }
            default:
            {
                displayRegistrationPage();
                break;
            }
        }
    }
    
    public static void showAppointmentSelection(int userSelection)
    {
        switch(userSelection)
        {
            case 1:
            {
                displayBookAppointmentPage();
                displayManageAppointmentPage();
                break;                
            }
            case 2:
            {
                displayUpdateAppointmentPage();
                displayManageAppointmentPage();
                break;
            }            
            case 98:
            {
                displayHomePage();
                break;
            }            
            case 99:
            {
                System.exit(0);
                break;
            }
            default:
            {
                displayManageAppointmentPage();
                break;
            }
        }
    }
    
    public static void showPetTreatmentSelection(int userSelection)
    {
        switch(userSelection)
        {
            case 1:
            {
                displayAddPetProductForTreatmentPage();
                displayManagePetTreatmentPage();
                break;                
            }
            case 2:
            {
                displayPrescribePetTreatmentPage();
                displayManagePetTreatmentPage();
                break;
            }            
            case 98:
            {
                displayHomePage();
                break;
            }            
            case 99:
            {
                System.exit(0);
                break;
            }
            default:
            {
                displayManagePetTreatmentPage();
                break;
            }
        }
    }
}