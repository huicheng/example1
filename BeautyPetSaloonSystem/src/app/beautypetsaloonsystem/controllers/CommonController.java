package app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.models.Appointment;
import app.beautypetsaloonsystem.models.Pet;
import app.beautypetsaloonsystem.models.PetOwner;
import app.beautypetsaloonsystem.models.PetSitter;
import app.beautypetsaloonsystem.models.PetSitterTimeSlot;
import app.beautypetsaloonsystem.models.TimeSlot;
import app.beautypetsaloonsystem.models.TimeSlotDate;
import app.beautypetsaloonsystem.views.ContinueView;
import app.beautypetsaloonsystem.views.RegistrationView;
import app.beautypetsaloonsystem.views.HomePageView;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author Kishani Joseph
 */
public class CommonController {
    private HomePageView homeView;
    private RegistrationView registerView;
    private ContinueView continueView;
    
    public CommonController()
    {
        this.homeView=new HomePageView();
        this.registerView=new RegistrationView();
        this.continueView=new ContinueView();
    }
    
    public boolean showHomePage()
    {
        return this.homeView.displayHomePageView();
    }
    
    public boolean showRegistrationPage()
    {
        return this.registerView.displayRegistrationView();
    }
    
    public boolean showContinuePage(String continueMessage)
    {
        return this.continueView.displayContinueView(continueMessage);
    }
    
    public int readUserSelection()
    {
        Scanner scanner = new Scanner(System.in);
        
        return scanner.hasNextInt() ? scanner.nextInt() : -1;     
    }
    
    public boolean readContinueSelection()
    {
        Scanner scanner = new Scanner(System.in);
        char continueSelection = scanner.next().charAt(0);
        
        return (continueSelection=='Y' || continueSelection =='y');
    }
    
    public void loadPreRegisteredData()
    {        
        PetSitterController petSitterController=new PetSitterController();
        PetSitter petSitter1=new PetSitter(petSitterController.getPetSitterList(), "Frances", "01/01/1957", "01012006", "Senior");
        petSitterController.getPetSitterList().add(petSitter1);
        PetSitter petSitter2=new PetSitter(petSitterController.getPetSitterList(), "Barbara", "07/11/1939", "01012008", "Junior");
        petSitterController.getPetSitterList().add(petSitter2);
        PetSitter petSitter3=new PetSitter(petSitterController.getPetSitterList(), "Shafrira", "14/11/1958", "01012012", "Trainig");
        petSitterController.getPetSitterList().add(petSitter3);
        
        PetOwnerController petOwnerController=new PetOwnerController();
        PetOwner petOwner1=new PetOwner(petOwnerController.getPetOwnerList(), "Johnathan", "02/09/1964", "24102014");
        petOwnerController.getPetOwnerList().add(petOwner1);
        PetOwner petOwner2=new PetOwner(petOwnerController.getPetOwnerList(), "Scott", "09/07/1956", "28071989");
        petOwnerController.getPetOwnerList().add(petOwner2);
        PetOwner petOwner3=new PetOwner(petOwnerController.getPetOwnerList(), "Gary", "10/11/1989", "24012015");
        petOwnerController.getPetOwnerList().add(petOwner3);
        
        PetController petController=new PetController();
        Pet pet1=new Pet(petController.getPetList(), "Daisy", "Dog", "Beagle", petOwner1, false);
        petController.getPetList().add(pet1);
        Pet pet2=new Pet(petController.getPetList(), "Hooch", "Dog", "Mastiff", petOwner2, false);
        petController.getPetList().add(pet2);
        Pet pet3=new Pet(petController.getPetList(), "JB", "Dog", "Pug", petOwner3, false);
        petController.getPetList().add(pet3);
        
        Pet pet4=new Pet(petController.getPetList(), "Tigger", "Cat", "Beagle", petOwner1, false);
        petController.getPetList().add(pet4);
        Pet pet5=new Pet(petController.getPetList(), "Luna", "Cat", "Mastiff", petOwner2, false);
        petController.getPetList().add(pet5);
        Pet pet6=new Pet(petController.getPetList(), "Leo", "Cat", "Pug", petOwner3, false);
        petController.getPetList().add(pet6);
        
        Pet pet7=new Pet(petController.getPetList(), "Rocky", "Parrot", "African Gray", petOwner1, false);
        petController.getPetList().add(pet7);
        Pet pet8=new Pet(petController.getPetList(), "Rosie", "Parrot", "Parakeet", petOwner2, false);
        petController.getPetList().add(pet8);
        Pet pet9=new Pet(petController.getPetList(), "Romeo", "Parrot", "Macaw", petOwner3, false);
        petController.getPetList().add(pet9);
        
        Pet pet10=new Pet(petController.getPetList(), "DJ", "Rabbit", "Mini Rex", petOwner3, false);
        petController.getPetList().add(pet10);
        
        TimeSlot timeSlot1=new TimeSlot(8, 9);
        TimeSlot timeSlot2=new TimeSlot(9, 10);
        TimeSlot timeSlot3=new TimeSlot(10, 11);
        TimeSlot timeSlot4=new TimeSlot(12, 13);
        TimeSlot timeSlot5=new TimeSlot(13, 14);
        
        Date date=new Date();
        try
        {
             date=new SimpleDateFormat("dd/MM/yyyy").parse("02/12/2017");
        }
        catch(Exception ex){}
        
        TimeSlotDate timeSlotDate1=new TimeSlotDate(timeSlot1, date);        
        TimeSlotDate timeSlotDate2=new TimeSlotDate(timeSlot2, date);
        TimeSlotDate timeSlotDate3=new TimeSlotDate(timeSlot3, date);        
        TimeSlotDate timeSlotDate4=new TimeSlotDate(timeSlot4, date);        
        TimeSlotDate timeSlotDate5=new TimeSlotDate(timeSlot5, date);
        
        PetSitterTimeSlot petSitterTimeSlot1=new PetSitterTimeSlot(petSitter1, timeSlotDate1);
        PetSitterTimeSlot petSitterTimeSlot2=new PetSitterTimeSlot(petSitter1, timeSlotDate2);
        PetSitterTimeSlot petSitterTimeSlot3=new PetSitterTimeSlot(petSitter1, timeSlotDate3);
        PetSitterTimeSlot petSitterTimeSlot4=new PetSitterTimeSlot(petSitter1, timeSlotDate4);
        PetSitterTimeSlot petSitterTimeSlot5=new PetSitterTimeSlot(petSitter1, timeSlotDate5);
        
        PetSitterTimeSlot petSitterTimeSlot6=new PetSitterTimeSlot(petSitter2, timeSlotDate1);
        PetSitterTimeSlot petSitterTimeSlot7=new PetSitterTimeSlot(petSitter2, timeSlotDate2);
        PetSitterTimeSlot petSitterTimeSlot8=new PetSitterTimeSlot(petSitter2, timeSlotDate3);
        PetSitterTimeSlot petSitterTimeSlot9=new PetSitterTimeSlot(petSitter2, timeSlotDate4);
        PetSitterTimeSlot petSitterTimeSlot10=new PetSitterTimeSlot(petSitter2, timeSlotDate5);
        
        PetSitterTimeSlot petSitterTimeSlot11=new PetSitterTimeSlot(petSitter3, timeSlotDate1);
        PetSitterTimeSlot petSitterTimeSlot12=new PetSitterTimeSlot(petSitter3, timeSlotDate2);
        PetSitterTimeSlot petSitterTimeSlot13=new PetSitterTimeSlot(petSitter3, timeSlotDate3);
        PetSitterTimeSlot petSitterTimeSlot14=new PetSitterTimeSlot(petSitter3, timeSlotDate4);
        PetSitterTimeSlot petSitterTimeSlot15=new PetSitterTimeSlot(petSitter3, timeSlotDate5);
        
        AppointmentController appointmentController=new AppointmentController();
        Appointment appointment1=new Appointment(appointmentController.getAppointmentList(), pet1, petSitter1, petSitterTimeSlot1, "Combing", appointmentController.getAppointmentStatus().BOOKED);
        appointmentController.getAppointmentList().add(appointment1);
        Appointment appointment2=new Appointment(appointmentController.getAppointmentList(), pet2, petSitter1, petSitterTimeSlot2, "Bath", appointmentController.getAppointmentStatus().BOOKED);
        appointmentController.getAppointmentList().add(appointment2);
        Appointment appointment3=new Appointment(appointmentController.getAppointmentList(), pet3, petSitter1, petSitterTimeSlot3, "Massage", appointmentController.getAppointmentStatus().BOOKED);
        appointmentController.getAppointmentList().add(appointment3);
        Appointment appointment4=new Appointment(appointmentController.getAppointmentList(), pet4, petSitter1, petSitterTimeSlot4, "Hair Trimmer", appointmentController.getAppointmentStatus().BOOKED);
        appointmentController.getAppointmentList().add(appointment4);
        Appointment appointment5=new Appointment(appointmentController.getAppointmentList(), pet5, petSitter1, petSitterTimeSlot5, "Combing", appointmentController.getAppointmentStatus().BOOKED);
        appointmentController.getAppointmentList().add(appointment5);
        Appointment appointment6=new Appointment(appointmentController.getAppointmentList(), pet6, petSitter2, petSitterTimeSlot6, "Bath", appointmentController.getAppointmentStatus().BOOKED);
        appointmentController.getAppointmentList().add(appointment6);
        Appointment appointment7=new Appointment(appointmentController.getAppointmentList(), pet7, petSitter2, petSitterTimeSlot7, "Massage", appointmentController.getAppointmentStatus().BOOKED);
        appointmentController.getAppointmentList().add(appointment7);
        Appointment appointment8=new Appointment(appointmentController.getAppointmentList(), pet8, petSitter2, petSitterTimeSlot8, "Hair Trimer", appointmentController.getAppointmentStatus().BOOKED);
        appointmentController.getAppointmentList().add(appointment8);
        Appointment appointment9=new Appointment(appointmentController.getAppointmentList(), pet9, petSitter2, petSitterTimeSlot9, "Combing", appointmentController.getAppointmentStatus().BOOKED);
        appointmentController.getAppointmentList().add(appointment9);
        Appointment appointment10=new Appointment(appointmentController.getAppointmentList(), pet10, petSitter2, petSitterTimeSlot10, "Bath", appointmentController.getAppointmentStatus().BOOKED);
        appointmentController.getAppointmentList().add(appointment10);
        Appointment appointment11=new Appointment(appointmentController.getAppointmentList(), pet10, petSitter3, petSitterTimeSlot11, "Bath", appointmentController.getAppointmentStatus().BOOKED);
        appointmentController.getAppointmentList().add(appointment11);
        Appointment appointment12=new Appointment(appointmentController.getAppointmentList(), pet9, petSitter3, petSitterTimeSlot12, "Massage", appointmentController.getAppointmentStatus().BOOKED);
        appointmentController.getAppointmentList().add(appointment12);
        Appointment appointment13=new Appointment(appointmentController.getAppointmentList(), pet8, petSitter3, petSitterTimeSlot13, "Hair Trimer", appointmentController.getAppointmentStatus().BOOKED);
        appointmentController.getAppointmentList().add(appointment13);
        Appointment appointment14=new Appointment(appointmentController.getAppointmentList(), pet7, petSitter3, petSitterTimeSlot14, "Combing", appointmentController.getAppointmentStatus().BOOKED);
        appointmentController.getAppointmentList().add(appointment14);
        Appointment appointment15=new Appointment(appointmentController.getAppointmentList(), pet6, petSitter3, petSitterTimeSlot15, "Bath", appointmentController.getAppointmentStatus().BOOKED);
        appointmentController.getAppointmentList().add(appointment15);
    }
}
