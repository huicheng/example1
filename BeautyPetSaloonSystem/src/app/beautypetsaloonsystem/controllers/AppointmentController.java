package app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.models.Appointment;
import app.beautypetsaloonsystem.models.MonthlyAppointmentReport;
import app.beautypetsaloonsystem.models.Pet;
import app.beautypetsaloonsystem.models.PetSitter;
import app.beautypetsaloonsystem.models.PetSitterTimeSlot;
import app.beautypetsaloonsystem.models.Validation;
import app.beautypetsaloonsystem.views.ManageAppointmentView;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class AppointmentController {
    private Appointment appointment;
    private MonthlyAppointmentReport monthlyAppointmentReport;
    private ManageAppointmentView manageAppointmentView;

    private static List<Appointment> appointmentList = new ArrayList<Appointment>();
    private static List<MonthlyAppointmentReport> monthlyAppointmentReportList = new ArrayList<MonthlyAppointmentReport>();
    
    public AppointmentController()
    {
        this.appointment=new Appointment();
        this.monthlyAppointmentReport=new MonthlyAppointmentReport();
        this.manageAppointmentView = new ManageAppointmentView();
    }
    
    public void setAppointmentID(List<Appointment> appointmentList)
    {
        this.appointment.setAppointmentID(appointmentList);
    }
      
    public Pet getPetForAppointment()
    {
        return this.appointment.getPet();
    }
    
    public void setPetForAppointment(Pet pet)
    {
        this.appointment.setPet(pet);
    }
    
    public PetSitter getPetSitterForAppointment()
    {
        return this.appointment.getPetSitter();
    }
    
    public void setPetSitterForAppointment(PetSitter petSitter)
    {
        this.appointment.setPetSitter(petSitter);
    }
    
    public PetSitterTimeSlot getPetSitterTimeSlotForAppointment()
    {
        return this.appointment.getPetSitterTimeSlot();
    }
    
    public void setPetSitterTimeSlotForAppointment(PetSitterTimeSlot petSitterTimeSlot)
    {
        this.appointment.setPetSitterTimeSlot(petSitterTimeSlot);
    }
    
    public Appointment.AppointmentStatus getAppointmentStatus()
    {
        return this.appointment.getAppointmentStatus();
    }
    
    public String getRequestedService()
    {
        return this.appointment.getRequestedService();
    }
    
    public void setRequestedService(String requestedService)
    {
        this.appointment.setRequestedService(requestedService);        
    }
    
    public List getAppointmentList()
    {
        return this.appointmentList;        
    }
    
    public List getMonthlyAppointmentReportList()
    {
        return this.monthlyAppointmentReportList;
    }
    
    public boolean showManageAppointmentPage()
    {
        return this.manageAppointmentView.displayManageAppointmentView();
    }
    
    public boolean showBookAppointmentPage()
    {
        return this.manageAppointmentView.displayBookAppointmentView();
    }
    
    public boolean showUpdateAppointmentPage()
    {
        return this.manageAppointmentView.displayUpdateAppointmentView();
    }
    
    public void saveAppointmentDetails()
    {
        boolean dataInSystem = true;
        
        PetController petController = new PetController();
        List petList = petController.getPetList();
        
        PetSitterController petSitterController = new PetSitterController();
        List petSitterList = petSitterController.getPetSitterList();
        
        if(petList.isEmpty() || petSitterList.isEmpty())
        {
            this.manageAppointmentView.displayNoDataMessage();
            dataInSystem=false;
        }
        
        if(dataInSystem)
        {        
            while(Validation.isInputNull(this.getPetForAppointment()))
            {
                petController.viewPetList();
                
                String userSelection0 = this.manageAppointmentView.displayPetField();
                int petSelection = 0;
                if(Validation.isValidNumber(userSelection0))
                {
                    petSelection = Integer.parseInt(userSelection0);
                }
            
                if(Validation.isIndexInList(petList.size(), petSelection))
                {
                    this.setPetForAppointment((Pet)petList.get(petSelection-1));
                }
            }
        
            while(Validation.isInputNull(this.getPetSitterForAppointment()))
            {
                petSitterController.viewPetSitterList();
                
                String userSelection1 = this.manageAppointmentView.displayPetSitterField();
                int petSitterSelection = 0;
                if(Validation.isValidNumber(userSelection1))
                {
                    petSitterSelection = Integer.parseInt(userSelection1);
                }
            
                if(Validation.isIndexInList(petSitterList.size(), petSitterSelection))
                {
                    this.setPetSitterForAppointment((PetSitter)petSitterList.get(petSitterSelection-1));
                    
                    while(Validation.isInputNull(this.getPetSitterTimeSlotForAppointment()))
                    {
                        PetSitterTimeSlotController petSitterTimeSlotController = new PetSitterTimeSlotController();
                        List petSitterTimeSlotList = petSitterTimeSlotController.getPetSitterTimeSlotsForPetSitter((PetSitter)petSitterList.get(petSitterSelection-1), petSitterTimeSlotController.getPetSitterTimeSlotList((PetSitter)petSitterList.get(petSitterSelection-1)));
                        petSitterTimeSlotController.viewPetSitterTimeSlotList((PetSitter)petSitterList.get(petSitterSelection-1));
                        
                        String userSelection2 = this.manageAppointmentView.displayPetSitterTimeSlotField();
                        int petSitterTimeSlotSelection = 0;
                        if(Validation.isValidNumber(userSelection2))
                        {
                            petSitterTimeSlotSelection = Integer.parseInt(userSelection2);
                        }
                    
                        if(Validation.isIndexInList(petSitterTimeSlotList.size(), petSitterTimeSlotSelection))
                        {
                            PetSitterTimeSlot petSitterTimeSlot = (PetSitterTimeSlot)petSitterTimeSlotList.get(petSitterTimeSlotSelection-1);
                            if(petSitterTimeSlot.getSlotAvailabilityStatus())
                            {
                                this.setPetSitterTimeSlotForAppointment((PetSitterTimeSlot)petSitterTimeSlotList.get(petSitterTimeSlotSelection-1));
                            }
                            else
                            {
                                this.manageAppointmentView.displayCannotBookMessage();
                            }
                        }
                    }
                }
            }
            
            while(Validation.isInputNull(this.getRequestedService()))
            {
                this.setRequestedService(this.manageAppointmentView.displayRequestedServiceField());
            }
        
            this.appointment.addObserver(this.getPetSitterTimeSlotForAppointment());
            this.appointment.setAppointmentStatus(Appointment.AppointmentStatus.BOOKED);
            this.appointment.setAppointmentID(AppointmentController.appointmentList);
            AppointmentController.appointmentList.add(this.appointment);
        }
    }
    
    public void updateAppointmentDetails()
    {
        boolean dataInSystem=true;
        boolean appointmentNotUpdated=true;
        
        if(AppointmentController.appointmentList.isEmpty())
        {
            this.manageAppointmentView.displayNoAppointmentMessage();
            dataInSystem=false;
        }
        
        if(dataInSystem)
        {
            while(appointmentNotUpdated)
            {
                this.appointment.displayAppointmentDetails(appointmentList);
                
                String userSelection0 = this.manageAppointmentView.displayAppointmentField();
                int appointmentSelection = 0;
                if(Validation.isValidNumber(userSelection0))
                {
                    appointmentSelection = Integer.parseInt(userSelection0);
                }
                
                if(Validation.isIndexInList(AppointmentController.appointmentList.size(), appointmentSelection))
                {
                    String userSelection = this.manageAppointmentView.displayUpdateOptions();
                    int statusSelection = 0;
                    if(Validation.isValidNumber(userSelection))
                    {
                        statusSelection = Integer.parseInt(userSelection);
                    }
                    
                    Appointment.AppointmentStatus oldAppointmentStatus=AppointmentController.appointmentList.get(appointmentSelection-1).getAppointmentStatus();
                    Appointment.AppointmentStatus newAppointmentStatus=Appointment.AppointmentStatus.AVAILABLE;
                    switch(statusSelection)
                    {
                        case 1:
                        {
                            newAppointmentStatus=Appointment.AppointmentStatus.ATTENDED;
                            AppointmentController.appointmentList.get(appointmentSelection-1).setAppointmentStatus(newAppointmentStatus);
                            appointmentNotUpdated=false;
                            break;
                        }
                        case 2:
                        {
                            newAppointmentStatus=Appointment.AppointmentStatus.UNATTENDED;
                            AppointmentController.appointmentList.get(appointmentSelection-1).setAppointmentStatus(newAppointmentStatus);
                            appointmentNotUpdated=false;
                            break;
                        }
                        case 3:
                        {
                            newAppointmentStatus=Appointment.AppointmentStatus.CANCELLED;
                            AppointmentController.appointmentList.get(appointmentSelection-1).setAppointmentStatus(newAppointmentStatus);
                            appointmentNotUpdated=false;
                            break;
                        }
                    }
                    
                    if(!appointmentNotUpdated)
                    {
                        Date date = AppointmentController.appointmentList.get(appointmentSelection-1).getPetSitterTimeSlot().getTimeSlotDate().getTimeSlotDate();
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(date);
                        int monthNo = calendar.get(Calendar.MONTH);
                        int yearNo = calendar.get(Calendar.YEAR);
                        
                        PetSitter petSitterInAppointment = AppointmentController.appointmentList.get(appointmentSelection-1).getPetSitter();
                        PetSitterController petSitterController = new PetSitterController();
                        
                        if(AppointmentController.monthlyAppointmentReportList.isEmpty() || isAppointmentReportNotCreated(monthNo, yearNo))
                        {
                            this.createMonthlyAppointmentReport();
                        }
                        
                        for(MonthlyAppointmentReport monthReport: monthlyAppointmentReportList)
                        {
                            if(monthReport.getMonthNo()==monthNo && monthReport.getYearNo()==yearNo)
                            {
                                monthReport.updatePetSitterInReport(false, monthNo, yearNo, monthReport, petSitterController.getPetSitterList());
                                monthReport.updateMonthlyReport(monthNo, yearNo, oldAppointmentStatus, newAppointmentStatus, monthReport, petSitterInAppointment);
                            }
                        }
                    }
                }
            }
        }
    }
    
    public boolean isAppointmentReportNotCreated(int monthNo, int yearNo)
    {
        for(MonthlyAppointmentReport monthReport: monthlyAppointmentReportList)
        {
            if(monthReport.getMonthNo()==monthNo && monthReport.getYearNo()==yearNo)
            {
                return true;
            }
        }
        return false;
    }
    
    public void createMonthlyAppointmentReport()
    {
        boolean reportNotInSystem = true;
        Calendar calendar = Calendar.getInstance();
        int monthNo = calendar.get(Calendar.MONTH);
        int yearNo = calendar.get(Calendar.YEAR);
        
        for(MonthlyAppointmentReport monthReport: monthlyAppointmentReportList)
        {
            if(monthReport.getMonthNo()==monthNo && monthReport.getYearNo()==yearNo)
            {
                reportNotInSystem=false;
            }
        }
        
        if(reportNotInSystem)
        {
            PetSitterController petSitterController=new PetSitterController();
            MonthlyAppointmentReport newMonthReport = new MonthlyAppointmentReport();
            newMonthReport.createMonthlyReport(monthNo, yearNo, newMonthReport);
            newMonthReport.updatePetSitterInReport(true, monthNo, yearNo, newMonthReport, petSitterController.getPetSitterList());
            this.monthlyAppointmentReportList.add(newMonthReport);
        }
    }
    
    public void updatePetSitterInMonthlyAppointmentReport()
    {
        Calendar calendar = Calendar.getInstance();
        int monthNo = calendar.get(Calendar.MONTH);
        int yearNo = calendar.get(Calendar.YEAR);
        
        PetSitterController petSitterController = new PetSitterController();
        for(MonthlyAppointmentReport monthReport: monthlyAppointmentReportList)
        {
            if(monthReport.getMonthNo()==monthNo && monthReport.getYearNo()==yearNo)
            {
                monthlyAppointmentReport.updatePetSitterInReport(false, monthNo, yearNo, monthReport, petSitterController.getPetSitterList());
            }
        }
    }
    
    public void viewAppointmentList()
    {        
        this.appointment.displayAppointmentDetails(this.appointmentList);
    }
    
    public void viewMonthlyAppointmentReport()
    {
        this.monthlyAppointmentReport.displayMonthlyAppointmentReport(monthlyAppointmentReportList);
    }
}