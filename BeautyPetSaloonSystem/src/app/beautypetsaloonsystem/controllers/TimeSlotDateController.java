package app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.models.TimeSlot;
import app.beautypetsaloonsystem.models.TimeSlotDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class TimeSlotDateController {
    private TimeSlotDate timeSlotDate;
    
    private static List<TimeSlotDate> timeSlotDateList = new ArrayList<TimeSlotDate>();
    
    public TimeSlotDateController()
    {
        this.timeSlotDate = new TimeSlotDate();
    }
    
    public List<TimeSlotDate> getTimeSlotDateList()
    {
        return this.timeSlotDateList;
    }
    
    public List<TimeSlotDate> loadTimeSlotDates(int appointmentDays, List<TimeSlot> timeSlotList, List<TimeSlotDate> timeSlotDateList)
    {
        return this.timeSlotDate.loadTimeSlotDates(appointmentDays, timeSlotList, timeSlotDateList);
    }
    
    public void updateTimeSlotDates()
    {
        if(this.timeSlotDateList.isEmpty())
        {
            TimeSlotController timeSlotController = new TimeSlotController();
            this.timeSlotDateList=this.loadTimeSlotDates(3, timeSlotController.getTimeSlotList(), this.timeSlotDateList);
        }
    }   
}
