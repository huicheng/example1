package app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.models.MonthlyProductOrderReport;
import app.beautypetsaloonsystem.models.PetProduct;
import app.beautypetsaloonsystem.models.ProductOrder;
import app.beautypetsaloonsystem.models.Validation;
import app.beautypetsaloonsystem.views.ProductOrderView;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class ProductOrderController {
    private ProductOrder productOrder;    
    private MonthlyProductOrderReport monthlyProductOrderReport;
    private ProductOrderView productOrderView;
    
    private static List<ProductOrder> productOrderList = new ArrayList<ProductOrder>();
    private static List<MonthlyProductOrderReport> monthlyProductOrderReportList = new ArrayList<MonthlyProductOrderReport>();
    
    public ProductOrderController()
    {
        this.productOrder=new ProductOrder();
        this.monthlyProductOrderReport=new MonthlyProductOrderReport();
        this.productOrderView=new ProductOrderView();
    }
    
    public int getProductOrderID()
    {
        return this.productOrder.getProductOrderID();
    }
    
    public void setProductOrderID(List<ProductOrder> productOrderList)
    {
        this.productOrder.setProductOrderID(productOrderList);
    }
    
    public PetProduct getPetProductForOrder()
    {
        return this.productOrder.getPetProductForOrder();
    }
    
    public void setPetProductForOrder(PetProduct petProduct)
    {
        this.productOrder.setPetProductForOrder(petProduct);
    }
    
    public String getOrderedQuantity()
    {
        return this.productOrder.getOrderedQuantity();
    }
    
    public void setOrderedQuantity(String orderQuantity)
    {
        this.productOrder.setOrderedQuantity(orderQuantity);
    }
    
    public Date getOrderedDate()
    {
        return this.productOrder.getOrderedDate();
    }
    
    public void setOrderedDate()
    {
        this.productOrder.setOrderedDate();
    }
    
    public boolean showProductOrderPage()
    {
        return this.productOrderView.displayProductOrderView();
    }
    
    public void saveProductOrderDetails()
    {
        boolean dataInSystem = true;
        
        PetProductController petProductController = new PetProductController();
        List petProductList = petProductController.getPetProductList();
        
        if(petProductList.isEmpty())
        {
            this.productOrderView.displayNoDataMessage();
            dataInSystem = false;
        }
        
        if(dataInSystem)
        {
            this.setProductOrderID(ProductOrderController.productOrderList);
        
            while(Validation.isInputNull(this.getPetProductForOrder()))
            {
                petProductController.viewPetProductList();
                String userSelection = this.productOrderView.displayPetProductField();
                int petProductSelection = 0;
                if(Validation.isValidNumber(userSelection))
                {
                    petProductSelection = Integer.parseInt(userSelection);
                }
            
                if(Validation.isIndexInList(petProductList.size(), petProductSelection))
                {
                    this.setPetProductForOrder((PetProduct)petProductList.get(petProductSelection-1));
                }
            }
        
            while(Validation.isInputNull(this.getOrderedQuantity()))
            {
                this.setOrderedQuantity(this.productOrderView.displayOrderQuantityField());
            }
        
            this.setOrderedDate();
        
            ProductOrderController.productOrderList.add(productOrder);
            
            Date date = productOrder.getOrderedDate();
            
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int monthNo = calendar.get(Calendar.MONTH);
            int yearNo = calendar.get(Calendar.YEAR);
            
            if(ProductOrderController.monthlyProductOrderReportList.isEmpty() || isProductOrderReportNotCreated(monthNo, yearNo))
            {
                this.createMonthlyProductOrderReport(); 
            }
            
            for(MonthlyProductOrderReport monthReport: ProductOrderController.monthlyProductOrderReportList)
            {
                if(monthReport.getMonthNo()==monthNo && monthReport.getYearNo()==yearNo)
                {
                    monthReport.updatePetProductInReport(false, monthNo, yearNo, monthReport, petProductList);
                    monthReport.updateMonthlyReport(monthNo, yearNo, monthReport, productOrder);
                }
            }
        }
    }
    
    public boolean isProductOrderReportNotCreated(int monthNo, int yearNo)
    {
        for(MonthlyProductOrderReport monthReport: ProductOrderController.monthlyProductOrderReportList)
        {
            if(monthReport.getMonthNo()==monthNo && monthReport.getYearNo()==yearNo)
            {
                return true;
            }
        }
        return false;
    }
    
    public void createMonthlyProductOrderReport()
    {
        boolean reportNotInSystem = true;
        Calendar calendar = Calendar.getInstance();
        int monthNo = calendar.get(Calendar.MONTH);
        int yearNo = calendar.get(Calendar.YEAR);
        
        for(MonthlyProductOrderReport monthReport: ProductOrderController.monthlyProductOrderReportList)
        {
            if(monthReport.getMonthNo()==monthNo && monthReport.getYearNo()==yearNo)
            {
                reportNotInSystem=false;
            }
        }
        
        if(reportNotInSystem)
        {
            PetProductController petProductController = new PetProductController();
            MonthlyProductOrderReport newMonthReport = new MonthlyProductOrderReport();
            newMonthReport.createMonthlyReport(monthNo, yearNo, newMonthReport);
            newMonthReport.updatePetProductInReport(true, monthNo, yearNo, newMonthReport, petProductController.getPetProductList());
            ProductOrderController.monthlyProductOrderReportList.add(newMonthReport);
        }
    }
    
    public void updatePetProductInMonthlyProductOrderReport()
    {
        Calendar calendar = Calendar.getInstance();
        int monthNo = calendar.get(Calendar.MONTH);
        int yearNo = calendar.get(Calendar.YEAR);
        
        PetProductController petProductController = new PetProductController();
        for(MonthlyProductOrderReport monthReport: ProductOrderController.monthlyProductOrderReportList)
        {
            if(monthReport.getMonthNo()==monthNo && monthReport.getYearNo()==yearNo)
            {
                monthReport.updatePetProductInReport(false, monthNo, yearNo, monthReport, petProductController.getPetProductList());
            }
        }
    }
    
    public void viewProductOrderList()
    {
        this.productOrder.displayProductOrderDetails(ProductOrderController.productOrderList);
    }
    
    public void viewMonthlyProductOrderReport()
    {
        this.monthlyProductOrderReport.displayMonthlyProductOrderReport(ProductOrderController.monthlyProductOrderReportList);
    }
}