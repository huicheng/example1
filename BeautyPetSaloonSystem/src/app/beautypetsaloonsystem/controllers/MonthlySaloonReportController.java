package app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.views.MonthlySaloonReportView;

/**
 *
 * @author Kishani Joseph
 */
public class MonthlySaloonReportController {
    private MonthlySaloonReportView monthlySaloonReportView;
    
    public MonthlySaloonReportController()
    {
        this.monthlySaloonReportView=new MonthlySaloonReportView();        
    }
    
    public boolean showMonthlySaloonReportPage()
    {
        return this.monthlySaloonReportView.displayMonthlySaloonReportView();
    }
    
    public void showMonthlySaloonReport()
    {
        AppointmentController appointmentController = new AppointmentController();
        appointmentController.createMonthlyAppointmentReport();
        appointmentController.updatePetSitterInMonthlyAppointmentReport();
        appointmentController.viewMonthlyAppointmentReport();
        
        ProductOrderController productOrderController = new ProductOrderController();
        productOrderController.createMonthlyProductOrderReport();
        productOrderController.updatePetProductInMonthlyProductOrderReport();
        productOrderController.viewMonthlyProductOrderReport();
    }    
}
