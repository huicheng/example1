package app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.models.TimeSlot;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class TimeSlotController {
    private TimeSlot timeSlot;
    
    private static List<TimeSlot> timeSlotList = new ArrayList<TimeSlot>();
    
    public TimeSlotController()
    {
        this.timeSlot = new TimeSlot();
    }
    
    public List<TimeSlot> getTimeSlotList()
    {
        return this.timeSlotList;
    }
    
    public List<TimeSlot> loadTimeSlots(int shiftStart, int shiftDuration, int slotDuration, List<TimeSlot> timeSlotList)
    {
        return this.timeSlot.loadTimeSlots(shiftStart, shiftDuration, slotDuration, timeSlotList);
    }
    
    public void updateTimeSlots()
    {
        if(this.timeSlotList.isEmpty())
        {
            this.timeSlotList=this.loadTimeSlots(9, 10, 1, this.timeSlotList);
        }
    }
}
