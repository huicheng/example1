package app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.models.PetProduct;
import app.beautypetsaloonsystem.models.PetTreatment;
import app.beautypetsaloonsystem.models.Validation;
import app.beautypetsaloonsystem.views.ManagePetTreatmentView;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class PetTreatmentController {
    private PetTreatment petTreatment;
    private ManagePetTreatmentView managePetTreatmentView;
    
    private static List<PetTreatment> petTreatmentList = new ArrayList<PetTreatment>();
    
    public PetTreatmentController()
    {
        this.petTreatment=new PetTreatment();
        this.managePetTreatmentView=new ManagePetTreatmentView();
    }
    
    public void setPetTreatmentID(List<PetTreatment> petTreatmentList)
    {
        this.petTreatment.setPetTreatmentID(petTreatmentList);
    }
    
    public String getTreatmentName()
    {
        return this.petTreatment.getTreatmentName();
    }
    
    public void setTreatmentName(String treatmentName)
    {
        this.petTreatment.setTreatmentName(treatmentName);        
    }
    
    public List getPetProductsForTreatment()
    {
        return this.petTreatment.getPetProductsForTreatment();
    }
    
    public void setPetProductsForTreatment(List<PetProduct> petProductList)
    {
        this.petTreatment.setPetProductsForTreatment(petProductList);
    }
    
    public boolean isProductDuplicate(PetProduct petProductSelected, PetTreatment petTreatment)
    {
        return this.petTreatment.isProductDuplicate(petProductSelected, petTreatment);
    }
    
    public List getPetTreatmentList()
    {
        return PetTreatmentController.petTreatmentList;        
    }
    
    public boolean showManagePetTreatmentPage()
    {
        return this.managePetTreatmentView.displayManagePetTreatmentView();
    }
    
    public boolean showRegisterPetTreatmentPage()
    {
        return this.managePetTreatmentView.displayRegisterPetTreatmentView();
    }
    
    public boolean showAddPetProductsForTreatmentPage()
    {
        return this.managePetTreatmentView.displayAddPetProductsForTreatmentView();
    }
    
    public void registerPetTreatmentDetails()
    {
        this.setPetTreatmentID(this.petTreatmentList);
        
        while(Validation.isInputNull(this.getTreatmentName()))
        {
            this.setTreatmentName(this.managePetTreatmentView.displayTreatmentNameField());
        }
        
        this.petTreatmentList.add(petTreatment);
    }
    
    public void addPetProductsForTreatment()
    {
        boolean dataInSystem = true;
        
        PetProductController petProductController = new PetProductController();
        List petProductList = petProductController.getPetProductList();        
        
        if(this.petTreatmentList.isEmpty() || petProductList.isEmpty())
        {
            this.managePetTreatmentView.displayNoDataMessageForAddProducts();
            dataInSystem=false;
        }
        
        if(dataInSystem)
        {
            PetTreatment petTreatment=null;      
            while(Validation.isInputNull(petTreatment))
            {
                this.viewPetTreatmentList();
                
                String userSelection0 = this.managePetTreatmentView.displayPetTreatmentField();
                int petTreatmentSelection = 0;
                if(Validation.isValidNumber(userSelection0))
                {
                    petTreatmentSelection = Integer.parseInt(userSelection0);
                }
            
                if(Validation.isIndexInList(petTreatmentList.size(), petTreatmentSelection))
                {
                    petTreatment=this.petTreatmentList.get(petTreatmentSelection-1);
                }
            }
        
            boolean productNotAdded=true;
            while(productNotAdded)
            {
                petProductController.viewPetProductList();
                
                String userSelection1 = this.managePetTreatmentView.displayPetProductField();
                int petProductSelection = 0;
                if(Validation.isValidNumber(userSelection1))
                {
                    petProductSelection = Integer.parseInt(userSelection1);
                }
            
                if(Validation.isIndexInList(petProductList.size(), petProductSelection))
                {
                    PetProduct petProduct=(PetProduct)petProductList.get(petProductSelection-1);
                    if(this.isProductDuplicate(petProduct, petTreatment))
                    {
                        this.managePetTreatmentView.displayProductDuplicateMessage();
                        productNotAdded=false;
                    }
                    else
                    {
                        petTreatment.getPetProductsForTreatment().add(petProduct);
                        this.viewPetTreatmentList();
                        productNotAdded=false;                        
                    }
                }
            }
        }
    }
        
    public void viewPetTreatmentList()
    {        
        this.petTreatment.displayPetTreatmentDetails(PetTreatmentController.petTreatmentList);
    }
}
