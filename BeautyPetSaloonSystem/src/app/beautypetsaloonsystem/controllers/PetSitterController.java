package app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.models.Person;
import app.beautypetsaloonsystem.models.PetSitter;
import app.beautypetsaloonsystem.models.Validation;
import app.beautypetsaloonsystem.views.CommonView;
import app.beautypetsaloonsystem.views.RegisterPetSitterView;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class PetSitterController {
    private PetSitter petSitter;
    private RegisterPetSitterView registerPetSitterView;
    private CommonView commonView;
    
    private static List<Person> petSitterList = new ArrayList<Person>();
    
    public PetSitterController()
    {
        this.petSitter = new PetSitter();
        this.registerPetSitterView = new RegisterPetSitterView();
        this.commonView = new CommonView();
    }
    
    public PetSitter getPetSitter()
    {
        return this.petSitter;
    }
    
    public void setPetSitterID(List<Person> petSitterList)
    {
        this.petSitter.setPersonID(petSitterList);
    }
    
    public String getPetSitterName()
    {
        return this.petSitter.getPersonName();
    }
    
    public void setPetSitterName(String name)
    {
        this.petSitter.setPersonName(name);
    }
    
    public Date getPetSitterDOB()
    {
        return this.petSitter.getPersonDOB();
    }
    
    public void setPetSitterDOB(String dateOfBirth)
    {
        this.petSitter.setPersonDOB(dateOfBirth);
    }
    
    public String getPetSitterContactNo()
    {
        return this.petSitter.getPersonContactNo();
    }
    
    public void setPetSitterContactNo(String contactNo)
    {
        this.petSitter.setPersonContactNo(contactNo);
    }
    
    public String getPetSitterDesignation()
    {
        return this.petSitter.getDesignationLevel();
    }
    
    public void setPetSitterDesgination(String designationLevel)
    {
        this.petSitter.setDesignationLevel(designationLevel);
    }
    
    public boolean getPetSitterNotInReport()
    {
        return this.petSitter.getPetSitterNotInReport();
    }
    
    public void setPetSitterNotInReport(boolean petSitterNotInReport)
    {
        this.petSitter.setPetSitterNotInReport(petSitterNotInReport);
    }
    
    public List<Person> getPetSitterList()
    {
        return PetSitterController.petSitterList;
    }
    
    public boolean showRegisterPetSitterPage()
    {
        return this.registerPetSitterView.displayRegisterPetSitterView();
    }
    
    public void registerPetSitterDetails()
    {
        this.setPetSitterID(PetSitterController.petSitterList);
        
        while(Validation.isInputNull(this.getPetSitterName()))
        {
            this.setPetSitterName(this.commonView.displayNameField());
        }
        
        while(Validation.isInputNull(this.getPetSitterDOB()))
        {
            this.setPetSitterDOB(this.commonView.displayDateField());
        }
        
        while(Validation.isInputNull(this.getPetSitterContactNo()))
        {
            this.setPetSitterContactNo(this.commonView.displayContactNoField());
        }
        
        while(Validation.isInputNull(this.getPetSitterDesignation()))
        {
            this.setPetSitterDesgination(this.registerPetSitterView.displayDesignationField());
        }
        
        this.setPetSitterNotInReport(true);
        PetSitterController.petSitterList.add(petSitter);
    }
    
    public void viewPetSitterList()
    {        
        this.petSitter.displayEmployeeDetails(petSitterList, "Pet Sitter List");
    }
}