package app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.models.PetProduct;
import app.beautypetsaloonsystem.models.Validation;
import app.beautypetsaloonsystem.views.RegisterPetProductView;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class PetProductController {
    private PetProduct petProduct;
    private RegisterPetProductView registerPetProductView;
        
    private static List<PetProduct> petProductList = new ArrayList<PetProduct>();
    
    public PetProductController()
    {
        this.petProduct=new PetProduct();
        this.registerPetProductView=new RegisterPetProductView();
    }
    
    public int getPetProductID()
    {
        return this.petProduct.getPetProductID();
    }
    
    public void setPetProductID(List<PetProduct> petProductList)
    {
        this.petProduct.setPetProductID(petProductList);
    }
    
    public String getPetProductName()
    {
        return this.petProduct.getProductName();
    }
    
    public void setPetProductName(String productName)
    {
        this.petProduct.setProductName(productName);
    }
    
    public String getQuantityInStock()
    {
        return this.petProduct.getQuantityInStock();
    }
    
    public void setQuantityInStock(String quantityInStock)
    {
        this.petProduct.setQuantityInStock(quantityInStock);
    }
    
    public boolean getStockAvailabilityStatus()
    {
        return this.petProduct.getStockAvailabilityStatus();
    }
    
    public void setStockAvailabilityStatus()
    {
        this.petProduct.setStockAvailabilityStatus();
    }
    
    public List getPetProductList()
    {
        return this.petProductList;        
    }
    
    public boolean getPetProductNotInReport()
    {
        return this.petProduct.getPetProductNotInReport();
    }
    
    public void setPetProductNotInReport(boolean petProductNotInReport)
    {
        this.petProduct.setPetProductNotInReport(petProductNotInReport);
    }
    
    public boolean showRegisterPetProductPage()
    {
        return this.registerPetProductView.displayRegisterPetProductView();
    }
    
    public void registerPetProductDetails()
    {
        this.setPetProductID(this.petProductList);
        
        while(Validation.isInputNull(this.getPetProductName()))
        {
            this.setPetProductName(this.registerPetProductView.displayProductNameField());
        }
        
        while(Validation.isInputNull(this.getQuantityInStock()))
        {
            this.setQuantityInStock(this.registerPetProductView.displayQuantityInStockField());
        }
        
        this.setStockAvailabilityStatus();
        this.setPetProductNotInReport(true);
                
        this.petProductList.add(petProduct);
    }
    
    public void viewPetProductList()
    {        
        this.petProduct.displayPetProductDetails(petProductList);
    }
}