package app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.models.PetSitter;
import app.beautypetsaloonsystem.models.PetSitterTimeSlot;
import app.beautypetsaloonsystem.models.TimeSlotDate;
import app.beautypetsaloonsystem.views.PetSitterTimeSlotView;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class PetSitterTimeSlotController {
    private PetSitterTimeSlot petSitterTimeSlot;
    private PetSitterTimeSlotView petSitterTimeSlotView;
    
    private static List<PetSitterTimeSlot> petSitterTimeSlotList = new ArrayList<PetSitterTimeSlot>();
    
    public PetSitterTimeSlotController()
    {
        this.petSitterTimeSlot = new PetSitterTimeSlot();
        this.petSitterTimeSlotView = new PetSitterTimeSlotView();
    }
    
    public boolean showTimeSlotDatePage()
    {
        return this.petSitterTimeSlotView.displayPetSitterTimeSlotView();
    } 
    
    public List<PetSitterTimeSlot> getPetSitterTimeSlotsForPetSitter(PetSitter petSitter, List<PetSitterTimeSlot> petSitterTimeSlotList)
    {
        return this.petSitterTimeSlot.getPetSitterTimeSlotsForPetSitter(petSitter, petSitterTimeSlotList);
    }
    
    public List<PetSitterTimeSlot> loadPetSitterTimeSlots(PetSitter petSitter, List<TimeSlotDate> timeSlotDateList, List<PetSitterTimeSlot> petSitterTimeSlotList)
    {
        return this.petSitterTimeSlot.loadPetSitterTimeSlots(petSitter, timeSlotDateList, petSitterTimeSlotList);
    }
    
    public List<PetSitterTimeSlot> getPetSitterTimeSlotList(PetSitter petSitter)
    {
        TimeSlotDateController timeSlotDateController = new TimeSlotDateController();
        this.petSitterTimeSlotList=this.loadPetSitterTimeSlots(petSitter, timeSlotDateController.getTimeSlotDateList(), this.petSitterTimeSlotList);
        
        return this.petSitterTimeSlotList;        
    }
    
    public void viewPetSitterTimeSlotList(PetSitter petSitter)
    {
        this.petSitterTimeSlot.displayPetSitterTimeSlotDateDetails(getPetSitterTimeSlotsForPetSitter(petSitter, getPetSitterTimeSlotList(petSitter)));
    }
}
