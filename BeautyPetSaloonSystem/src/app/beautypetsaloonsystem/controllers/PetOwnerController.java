package app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.models.Person;
import app.beautypetsaloonsystem.models.PetOwner;
import app.beautypetsaloonsystem.models.Validation;
import app.beautypetsaloonsystem.views.CommonView;
import app.beautypetsaloonsystem.views.RegisterPetOwnerView;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class PetOwnerController {
    private PetOwner petOwner;
    private RegisterPetOwnerView registerPetOwnerView;
    private CommonView commonView;
    
    private static List<Person> petOwnerList = new ArrayList<Person>();
    
    public PetOwnerController()
    {
        this.petOwner = new PetOwner();
        this.registerPetOwnerView = new RegisterPetOwnerView();
        this.commonView = new CommonView();
    }
    
    public void setPetOwnerID(List<Person> petOwnerList)
    {
        this.petOwner.setPersonID(petOwnerList);
    }
    
    public String getPetOwnerName()
    {
        return this.petOwner.getPersonName();
    }
    
    public void setPetOwnerName(String name)
    {
        this.petOwner.setPersonName(name);
    }
    
    public Date getPetOwnerDOB()
    {
        return this.petOwner.getPersonDOB();
    }
    
    public void setPetOwnerDOB(String dateOfBirth)
    {
        this.petOwner.setPersonDOB(dateOfBirth);
    }
    
    public String getPetOwnerContactNo()
    {
        return this.petOwner.getPersonContactNo();
    }
    
    public void setPetOwnerContactNo(String contactNo)
    {
        this.petOwner.setPersonContactNo(contactNo);
    }
    
    public Person getPetOwner(int petOwnerID)
    {
        return this.petOwnerList.get(petOwnerID-1);        
    }
    
    public List getPetOwnerList()
    {
        return this.petOwnerList;        
    }
    
    public boolean showRegisterPetOwnerPage()
    {
        return this.registerPetOwnerView.displayRegisterPetOwnerView();
    }
    
    public void registerPetOwnerDetails()
    {
        this.setPetOwnerID(this.petOwnerList);
        
        while(Validation.isInputNull(this.getPetOwnerName()))
        {
            this.setPetOwnerName(this.commonView.displayNameField());
        }
        
        while(Validation.isInputNull(this.getPetOwnerDOB()))
        {
            this.setPetOwnerDOB(this.commonView.displayDateField());
        }
        
        while(Validation.isInputNull(this.getPetOwnerContactNo()))
        {
            this.setPetOwnerContactNo(this.commonView.displayContactNoField());
        }
                
        this.petOwnerList.add(petOwner);
    }
    
    public void viewPetOwnerList()
    {        
        this.petOwner.displayPersonDetails(petOwnerList, "Pet Owner List");
    }    
}
