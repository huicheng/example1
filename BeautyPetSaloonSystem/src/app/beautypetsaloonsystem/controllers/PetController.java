package app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.models.Pet;
import app.beautypetsaloonsystem.models.PetOwner;
import app.beautypetsaloonsystem.models.PetProduct;
import app.beautypetsaloonsystem.models.PetTreatment;
import app.beautypetsaloonsystem.models.Validation;
import app.beautypetsaloonsystem.views.ListView;
import app.beautypetsaloonsystem.views.ModifyPetNoteView;
import app.beautypetsaloonsystem.views.RegisterPetView;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class PetController {
    private Pet pet;
    private RegisterPetView registerPetView;
    private ModifyPetNoteView modifyPetNoteView;
    private ListView listView;
    
    private static List<Pet> petList = new ArrayList<Pet>();
    
    public PetController()
    {
        this.pet = new Pet();
        this.registerPetView=new RegisterPetView();
        this.modifyPetNoteView=new ModifyPetNoteView();
        this.listView=new ListView();
    }
    
    public void setPetID(List<Pet> petList)
    {
        this.pet.setPetID(petList);
    }
    
    public String getPetName()
    {
        return this.pet.getPetName();
    }
    
    public void setPetName(String petName)
    {
        this.pet.setPetName(petName);
    }
    
    public String getPetCategory()
    {
        return this.pet.getPetCategory();
    }
    
    public void setPetCategory(String petCategory)
    {
        this.pet.setPetCategory(petCategory);
    }
    
    public String getPetBreed()
    {
        return this.pet.getPetBreed();
    }
    
    public void setPetBreed(String petBreed)
    {
        this.pet.setPetBreed(petBreed);
    }
    
    public String getPetNote()
    {
        return this.pet.getPetNote();
    }
    
    public void setPetNote(String petNote)
    {
        this.pet.setPetNote(petNote);
    }
    
    public PetOwner getPetOwnerForPet()
    {
        return this.pet.getPetOwnerForPet();
    }
    
    public void setPetOwnerForPet(PetOwner petOwner)
    {
        this.pet.setPetOwnerForPet(petOwner);
    }
    
    public List getPetList()
    {
        return PetController.petList;        
    }
    
    public boolean showRegisterPetPage()
    {
        return this.registerPetView.displayRegisterPetView();
    }
    
    public boolean showModifyPetNotePage()
    {
        return this.modifyPetNoteView.displayModifyPetNoteView();
    }
    
    public boolean showCurrentProductListPage()
    {
        return this.listView.displayCurrentProductListView();
    }
    
    public void registerPetDetails()
    {
        boolean dataInSystem = true;
        
        PetOwnerController petOwnerController = new PetOwnerController();
        List petOwnerList = petOwnerController.getPetOwnerList();
        
        if(petOwnerList.isEmpty())
        {
            this.registerPetView.displayNoDataMessage();
            dataInSystem = false;
        }
        
        if(dataInSystem)
        {
            this.setPetID(PetController.petList);
        
            while(Validation.isInputNull(this.getPetName()))
            {
                this.setPetName(this.registerPetView.displayPetNameField());
            }
            
            while(Validation.isInputNull(this.getPetCategory()))
            {
                this.setPetCategory(this.registerPetView.displayPetCategoryField());
            }
        
            while(Validation.isInputNull(this.getPetBreed()))
            {
                this.setPetBreed(this.registerPetView.displayPetBreedField());
            }
            
            while(Validation.isInputNull(this.getPetOwnerForPet()))
            {
                petOwnerController.viewPetOwnerList();
                String userSelection = this.registerPetView.displayPetOwnerField();
                int petOwnerSelection = 0;
                if(Validation.isValidNumber(userSelection))
                {
                    petOwnerSelection = Integer.parseInt(userSelection);
                }
            
                if(Validation.isIndexInList(petOwnerList.size(), petOwnerSelection))
                {
                    this.setPetOwnerForPet((PetOwner)petOwnerList.get(petOwnerSelection-1));
                }
            }
        
            PetController.petList.add(this.pet);
        }
    }
    
    public void modifyPetNote()
    {
        boolean dataInSystem = true;
        
        if(PetController.petList.isEmpty())
        {
            this.modifyPetNoteView.displayNoDataMessage();
            dataInSystem = false;
        }
        
        if(dataInSystem)
        {
            Pet petToModify=null;
            while(Validation.isInputNull(petToModify))
            {
                this.viewPetList();
                String userSelection = this.modifyPetNoteView.displayPetField();
                int petSelection = 0;
                if(Validation.isValidNumber(userSelection))
                {
                    petSelection = Integer.parseInt(userSelection);
                }
            
                if(Validation.isIndexInList(petList.size(), petSelection))
                {
                    petToModify=((Pet)petList.get(petSelection-1));
                }
            }
            
            petToModify.setPetNote(this.modifyPetNoteView.displayPetNoteField());
            this.viewPetList();
        }       
    }
    
    public void viewPetList()
    {        
        this.pet.displayPetDetails(PetController.petList);
    }
    
    public void viewCurrentProductList()
    {
        PetTreatmentController petTreatmentController=new PetTreatmentController();
        List<PetTreatment> petTreatmentList=petTreatmentController.getPetTreatmentList();
        
        PetProductController petProductController=new PetProductController();
        List<PetProduct> petProductList=petProductController.getPetProductList();
        
        if(PetController.petList.isEmpty() || petTreatmentList.isEmpty() || petProductList.isEmpty())
        {
            this.listView.displayCurrentProductNoDataMessage();
        }
        else
        {
            this.pet.displayCurrentProductList(PetController.petList);
        }
    }
}