package app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.models.Pet;
import app.beautypetsaloonsystem.models.PetTreatment;
import app.beautypetsaloonsystem.models.Validation;
import app.beautypetsaloonsystem.views.ManagePetTreatmentView;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class PrescribePetTreatmentController {
    private Pet pet;
    private PetTreatment petTreatment;
    private ManagePetTreatmentView managePetTreatmentView;
    
    public PrescribePetTreatmentController()
    {
        this.pet=new Pet();
        this.petTreatment=new PetTreatment();
        this.managePetTreatmentView=new ManagePetTreatmentView();
    }
    
    public boolean isPetTreatmentDuplicate(PetTreatment petTreatmentSelected, Pet pet)
    {
        return this.pet.isPetTreatmentDuplicate(petTreatmentSelected, pet);
    }
    
    public void prescribePetTreatments()
    { 
        boolean dataInSystem = true;
        
        PetController petController = new PetController();
        List petList = petController.getPetList();
        
        PetTreatmentController petTreatmentController = new PetTreatmentController();
        List petTreatmentList = petTreatmentController.getPetTreatmentList();
        
        if(petList.isEmpty() || petTreatmentList.isEmpty())
        {
            this.managePetTreatmentView.displayNoDataMessageForPrescribeTreatments();
            dataInSystem=false;
        }
        
        if(dataInSystem)
        {
            Pet petToPrescribe=null;
            PetTreatment petTreatmentToBePrescribed=null;
            
            while(Validation.isInputNull(petToPrescribe))
            {
                petController.viewPetList();
                
                String userSelection0 = this.managePetTreatmentView.displayPetField();
                int petSelection = 0;
                if(Validation.isValidNumber(userSelection0))
                {
                    petSelection = Integer.parseInt(userSelection0);
                }
            
                if(Validation.isIndexInList(petList.size(), petSelection))
                {
                    petToPrescribe=((Pet)petList.get(petSelection-1));
                }
            }
        
            boolean treatmentNotAdded=true;
            while(treatmentNotAdded)
            {
                petTreatmentController.viewPetTreatmentList();
                
                String userSelection1 = this.managePetTreatmentView.displayPetTreatmentField();
                int petTreatmentSelection = 0;
                if(Validation.isValidNumber(userSelection1))
                {
                    petTreatmentSelection = Integer.parseInt(userSelection1);
                }
            
                if(Validation.isIndexInList(petTreatmentList.size(), petTreatmentSelection))
                {
                    petTreatmentToBePrescribed=(PetTreatment)petTreatmentList.get(petTreatmentSelection-1);
                    if(this.isPetTreatmentDuplicate(petTreatmentToBePrescribed, petToPrescribe))
                    {
                        this.managePetTreatmentView.displayPetTreatmentDuplicateMessage();
                        treatmentNotAdded=false;
                    }
                    else
                    {
                        petToPrescribe.getPetTreatmentsForPet().add(petTreatmentToBePrescribed);
                        petToPrescribe.displayPrescribedPetTreatmentDetails(petController.getPetList());
                        treatmentNotAdded=false;                        
                    }                    
                }
            }
        }
    }
    
    public boolean showPrescribePetTreatmentPage()
    {
        return this.managePetTreatmentView.displayPrescribePetTreatmentView();
    }
}
