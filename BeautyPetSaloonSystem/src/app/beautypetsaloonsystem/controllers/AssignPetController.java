package app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.models.Pet;
import app.beautypetsaloonsystem.models.PetSitter;
import app.beautypetsaloonsystem.models.Validation;
import app.beautypetsaloonsystem.views.AssignPetView;
import java.util.List;

/**
 *
 * @author Kishani Joseph
 */
public class AssignPetController {
    private Pet pet;
    private PetSitter petSitter;
    private AssignPetView assignPetView;
    
    public AssignPetController()
    {
        this.pet=new Pet();
        this.petSitter=new PetSitter();
        this.assignPetView=new AssignPetView();
    }
    
    public boolean hasPetUnassignedInList(List<Pet> petList)
    {
        return this.pet.hasPetUnassignedInList(petList);
    }
    
    public List getPetAssignedList()
    {
        return this.petSitter.getPetAssignedList();
    }
    
    public boolean showAssignPetPage()
    {
        return this.assignPetView.displayAssignPetView();
    }
    
    public void assignPet()
    {
        boolean dataInSystem = true;
        
        PetController petController = new PetController();
        List petList = petController.getPetList();
        
        PetSitterController petSitterController = new PetSitterController();
        List petSitterList = petSitterController.getPetSitterList();
        
        if(petList.isEmpty() || petSitterList.isEmpty())
        {
            this.assignPetView.displayNoDataMessage();
            dataInSystem=false;            
        }
        
        if(dataInSystem)
        {
            Pet petToAssign=null;
            PetSitter petSitterToBeAssigned=null;
            if(this.hasPetUnassignedInList(petList))
            {
                while(Validation.isInputNull(petToAssign))
                {
                    petController.viewPetList();
                    
                    String userSelection0 = this.assignPetView.displayPetField();
                    int petSelection = 0;
                    
                    if(Validation.isValidNumber(userSelection0))
                    {
                        petSelection = Integer.parseInt(userSelection0);
                    }
                    
                    if(Validation.isIndexInList(petList.size(), petSelection))
                    {
                        Pet pet=(Pet)petList.get(petSelection-1);
                        if(pet.getAssignedToPetSitter())
                        {
                            this.assignPetView.displayCannotAssignMessage();
                        }
                        else
                        {
                            petToAssign=pet;
                        }
                    }
                }
                
                while(Validation.isInputNull(petSitterToBeAssigned))
                {
                    petSitterController.viewPetSitterList();
                    
                    String userSelection1 = this.assignPetView.displayPetSitterField();
                    int petSitterSelection = 0;
                    if(Validation.isValidNumber(userSelection1))
                    {
                        petSitterSelection = Integer.parseInt(userSelection1);
                    }
                    
                    if(Validation.isIndexInList(petSitterList.size(), petSitterSelection))
                    {
                        petSitterToBeAssigned=(PetSitter)petSitterList.get(petSitterSelection-1);
                    }
                }
                
                petToAssign.setAssignedToPetSitter(true);
                petSitterToBeAssigned.setPetAssignedList(petToAssign);
                petSitterToBeAssigned.displayPetAssignedDetails(petSitterController.getPetSitterList());
            }
            else
            {
                this.assignPetView.displayNoPetToAssign();
            }
        }
    }
}
