package app.beautypetsaloonsystem.tests.app.beautypetsaloonsystem.models;

import app.beautypetsaloonsystem.models.Person;
import app.beautypetsaloonsystem.models.Pet;
import app.beautypetsaloonsystem.models.PetOwner;
import app.beautypetsaloonsystem.models.PetTreatment;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;

/**
 *
 * @author Kishani Joseph
 */
public class PetTest {
    private Pet pet;
    private Pet pet0;
    private Pet pet1;
    private PetTreatment petTreatment0;
    private PetTreatment petTreatment1;    
    private List<Person> personList;
    private List<Pet> petList;
    private List<PetTreatment> petTreatmentList;
    private List<PetTreatment> petTreatmentList0;
    private List<PetTreatment> petTreatmentList1;
    
    public PetTest() {
    }

    @Before
    public void beforeMethod()
    {
        pet=new Pet();
        personList=new ArrayList<Person>();
        petList=new ArrayList<Pet>();
        petTreatmentList=new ArrayList<PetTreatment>();
        petTreatmentList0=new ArrayList<PetTreatment>();
        petTreatmentList1=new ArrayList<PetTreatment>();
        
        petTreatment0=new PetTreatment(petTreatmentList, "Hair Trimming");
        petTreatmentList.add(petTreatment0); //to calculate petTreatmentID correctly
        petTreatmentList0.add(petTreatment0); //treatments prescribed for pet0
        
        petTreatment1=new PetTreatment(petTreatmentList, "Combing");
        petTreatmentList.add(petTreatment1); //to calculate petTreatmentID correctly
        petTreatmentList1.add(petTreatment1); //treatments prescribed for pet1
        
        pet0=new Pet(petList, "Daisy", "Dog", "Beagle", new PetOwner(personList, "Johnathan", "02/09/1964", "24102014"), false);
        pet0.setPetTreatmentsForPet(petTreatmentList0);
        
        pet1=new Pet(petList, "Hooch", "Dog", "Mastiff", new PetOwner(personList, "Scott", "09/07/1956", "28071989"), true);
        pet1.setPetTreatmentsForPet(petTreatmentList1);
    }
    
    @Test
    public void testIsPetTreatmentDuplicate()
    {
       assertTrue(pet.isPetTreatmentDuplicate(petTreatment0, pet0));
       assertFalse(pet.isPetTreatmentDuplicate(petTreatment1, pet0));
       
       assertTrue(pet.isPetTreatmentDuplicate(petTreatment1, pet1));
       assertFalse(pet.isPetTreatmentDuplicate(petTreatment0, pet1));
    }    
}
