package app.beautypetsaloonsystem.tests.app.beautypetsaloonsystem.models;

import app.beautypetsaloonsystem.models.Person;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Kishani Joseph
 */
public class PersonTest {
    
    public PersonTest() {}

    @Test
    public void testRetrievePersonID()
    {
        List<Person> personList=new ArrayList<Person>();
        Person person1=new Person(personList, "Frances", "01/01/1957", "01012006");        
        assertEquals(1, person1.retrievePersonID(personList));
        personList.add(person1); //list.get(0).getPersonID()=1      
        
        Person person2=new Person(personList, "Barbara", "07/11/1939", "01012008");
        assertEquals(2, person2.retrievePersonID(personList));        
        personList.add(person2); //list.get(1).getPersonID()=2
        
        personList.remove(0); //removing element should still give max number           
        Person person3=new Person(personList, "Shafrira", "14/11/1958", "01012012");
        assertEquals(3, person3.retrievePersonID(personList));
        personList.add(person3); //list.get(1).getPersonID()=3 
        
        personList.remove(1); //removing last element should give that id as max
        assertEquals(3, person1.retrievePersonID(personList));
        personList.add(person3); //list.get(1).getPersonID()=3 
    }
}