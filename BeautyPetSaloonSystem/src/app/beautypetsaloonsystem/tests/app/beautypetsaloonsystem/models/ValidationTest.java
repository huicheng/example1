package app.beautypetsaloonsystem.tests.app.beautypetsaloonsystem.models;

import app.beautypetsaloonsystem.models.Validation;
import java.text.SimpleDateFormat;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kishani Joseph
 */
public class ValidationTest {
    
    public ValidationTest() {}

    @Test
    public void testIsInuptNull() {
        String nullStr=null;
        assertTrue(Validation.isInputNull(null));
        assertTrue(Validation.isInputNull(nullStr));
        
        assertFalse(Validation.isInputNull("This is not null."));        
        assertFalse(Validation.isInputNull(new Object()));
    }
    
    @Test
    public void testIsValidString() {
        assertTrue(Validation.isValidString("a"));
        assertTrue(Validation.isValidString("this is valid"));
        assertTrue(Validation.isValidString("VALID"));
        
        assertFalse(Validation.isValidString(null));
        assertFalse(Validation.isValidString(new String()));
        assertFalse(Validation.isValidString("1 not valid"));
    }
    
    @Test
    public void testIsValidNumber()
    {
        assertTrue(Validation.isValidNumber("1"));
        assertTrue(Validation.isValidNumber("0000"));
        assertTrue(Validation.isValidNumber("011234567889"));
        
        assertFalse(Validation.isValidNumber("1-1"));
        assertFalse(Validation.isValidNumber("1a99"));
        assertFalse(Validation.isValidNumber("notvalidnumber"));
    }
    
    @Test
    public void testIndexInList()
    {
        assertTrue(Validation.isIndexInList(9, 8));
        assertTrue(Validation.isIndexInList(120, 119));
        assertTrue(Validation.isIndexInList(9, 9));
        
        assertFalse(Validation.isIndexInList(-9, -9));
        assertFalse(Validation.isIndexInList(99, 100));
        assertFalse(Validation.isIndexInList(0, 0));
    }
    
    @Test
    public void testIsValidDOB()
    {
        try
        {
            //for current date 01/02/2018, max birth date is 02/02/1938
            assertTrue(Validation.isValidDOB(new SimpleDateFormat("dd/MM/yyyy").parse("02/02/1938")));            
            //for current date 01/12/2017, min birth date is 01/12/1999
            assertTrue(Validation.isValidDOB(new SimpleDateFormat("dd/MM/yyyy").parse("01/12/1999")));

            //for current date 01/12/2017, max birth date is 02/12/1937
            assertFalse(Validation.isValidDOB(new SimpleDateFormat("dd/MM/yyyy").parse("01/12/1937")));
            //for current date 01/02/2018, min birth date is 01/02/2000
            assertFalse(Validation.isValidDOB(new SimpleDateFormat("dd/MM/yyyy").parse("01/02/2000")));
        }
        catch(Exception ex){}
    }
    
    @Test
    public void testIsValidContactNo()
    {
        assertTrue(Validation.isValidContactNo("123456789"));
        assertTrue(Validation.isValidContactNo("123456-"));
        assertTrue(Validation.isValidContactNo("-------"));
        
        assertFalse(Validation.isValidContactNo("123456"));        
        assertFalse(Validation.isValidContactNo("123-56"));
        assertFalse(Validation.isValidContactNo("12345678a"));
    }
}