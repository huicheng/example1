package app.beautypetsaloonsystem.tests.app.beautypetsaloonsystem.models;

import app.beautypetsaloonsystem.models.PetProduct;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Kishani Joseph
 */
public class PetProductTest {
    private List<PetProduct> petProductList;
    private PetProduct petProduct;
    private PetProduct availableProduct;
    private PetProduct unavailableProduct;
    
    public PetProductTest() {}
    
    @Before
    public void beforeMethod()
    {
        petProductList=new ArrayList<PetProduct>();
        petProduct=new PetProduct();
        availableProduct=new PetProduct(petProductList, "BarkLogic", "1", false);
        unavailableProduct=new PetProduct(petProductList, "EarthBath", "0", false);       
    }
    
    @Test
    public void testDisplayStockAvailability()
    {
        //verify stock status is available if product quantity is greater than zero
        assertEquals("AVAILABLE", petProduct.displayStockAvailability(availableProduct.getStockAvailabilityStatus()));
        //verify stock status is unavailable if product quantity is zero
        assertEquals("UNAVAILABLE", petProduct.displayStockAvailability(unavailableProduct.getStockAvailabilityStatus()));
    }
}
