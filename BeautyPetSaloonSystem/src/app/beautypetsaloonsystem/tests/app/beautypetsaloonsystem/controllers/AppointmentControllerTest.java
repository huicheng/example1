package app.beautypetsaloonsystem.tests.app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.controllers.AppointmentController;
import app.beautypetsaloonsystem.models.MonthlyAppointmentReport;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Kishani Joseph
 */
public class AppointmentControllerTest {
    private MonthlyAppointmentReport novReport;
    private MonthlyAppointmentReport decReport;
    private AppointmentController appointmentController;
        
    public AppointmentControllerTest() {}
    
    @Before
    public void beforeMethod()
    {
        //Calendar.MONTH for November is 10
        novReport=new MonthlyAppointmentReport(10, 2017);        
        //Calendar.MONTH for December is 11
        decReport=new MonthlyAppointmentReport(11, 2017);
        
        appointmentController=new AppointmentController();
        appointmentController.getMonthlyAppointmentReportList().add(novReport);
    }
    
    @Test
    public void testIsAppointmentReportNotCreated()
    {
        //verify false is returned when checking if December report is created
        assertFalse(appointmentController.isAppointmentReportNotCreated(11, 2017));
        
        //verify true is returned when checking if November report is created
        assertTrue(appointmentController.isAppointmentReportNotCreated(10, 2017));
        
        appointmentController.getMonthlyAppointmentReportList().add(decReport);
        //verify true is returned when checking if December report is created
        assertTrue(appointmentController.isAppointmentReportNotCreated(11, 2017));
    }
}
