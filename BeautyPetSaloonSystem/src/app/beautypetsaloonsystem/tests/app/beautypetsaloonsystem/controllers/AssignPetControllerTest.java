package app.beautypetsaloonsystem.tests.app.beautypetsaloonsystem.controllers;

import app.beautypetsaloonsystem.controllers.AssignPetController;
import app.beautypetsaloonsystem.models.Person;
import app.beautypetsaloonsystem.models.Pet;
import app.beautypetsaloonsystem.models.PetOwner;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Kishani Joseph
 */
public class AssignPetControllerTest {
    private AssignPetController assignPetController;
    private List<Person> personList;
    private List<Pet> petList;
    
    public AssignPetControllerTest() {}

    @Before
    public void beforeMethod()
    {
        assignPetController=new AssignPetController();
        personList=new ArrayList<Person>();
        petList=new ArrayList<Pet>();
        petList.add(new Pet(petList, "Daisy", "Dog", "Beagle", new PetOwner(personList, "Johnathan", "02/09/1964", "24102014"), false));
        petList.add(new Pet(petList, "Hooch", "Dog", "Mastiff", new PetOwner(personList, "Scott", "09/07/1956", "28071989"), true));
    }
    
    @Test
    public void testHasPetUnassignedInList() {
        //verify true if at least one pet has not been assigned to a pet sitter
        assertTrue(assignPetController.hasPetUnassignedInList(petList));
        
        petList.get(0).setAssignedToPetSitter(true);
        //verify false if no pet is left be assigned to a pet sitter
        assertFalse(assignPetController.hasPetUnassignedInList(petList));
    }
}