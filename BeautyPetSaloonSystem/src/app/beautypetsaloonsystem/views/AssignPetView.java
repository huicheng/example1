package app.beautypetsaloonsystem.views;

import java.util.Scanner;

/**
 *
 * @author Kishani Joseph
 */
public class AssignPetView {
    public boolean displayAssignPetView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                               Assign Pet to Pet Sitter                                                ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------"); 
        
        return true;
    }
    
    public String displayPetField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Pet (please select an option from the list): ");
        String petOwnerSelection = scanner.nextLine();
        
        System.out.println();
        
        return petOwnerSelection;
    }
    
    
    public String displayPetSitterField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Pet Sitter (please select an option from the list): ");
        String petOwnerSelection = scanner.nextLine();
        
        System.out.println();
        
        return petOwnerSelection;
    }
    
    public void displayNoDataMessage()
    {
        System.out.println("Sorry! Pet cannot be registered with pet sitter. Need at least one pet and one pet sitter.");
    }
    
    public void displayNoPetToAssign()
    {
        System.out.println("Sorry! Currently, all pets have already been assigned to a pet sitter.");
    }
    
    public void displayCannotAssignMessage()
    {
        System.out.println("Sorry! Pet cannot be assigned. Pet is already assigned to a pet sitter.");
    }
}