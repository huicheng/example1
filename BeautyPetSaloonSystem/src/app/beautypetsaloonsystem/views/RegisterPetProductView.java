package app.beautypetsaloonsystem.views;

import java.util.Scanner;

/**
 *
 * @author Kishani Joseph
 */
public class RegisterPetProductView {
    public boolean displayRegisterPetProductView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                  Register Pet Product                                                 ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------"); 
        
        return true;
    }
    
    public String displayProductNameField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Product Name: ");
        String name = scanner.nextLine();
        
        return name;
    }
    
    public String displayQuantityInStockField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Quantity In Stock: ");
        String name = scanner.nextLine();
        
        return name;
    }
}
