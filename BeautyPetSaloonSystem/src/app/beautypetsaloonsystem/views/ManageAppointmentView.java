package app.beautypetsaloonsystem.views;

import java.util.Scanner;

/**
 *
 * @author Kishani Joseph
 */
public class ManageAppointmentView {
    public boolean displayManageAppointmentView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                  Manage Appointments                                                  ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        
        System.out.println("What would you like to do?");
        System.out.println("1. Book Appointment");
        System.out.println("2. Update Appointment");
        System.out.println("98. Home");
        System.out.println("99. Exit");
        System.out.println();
        System.out.print("Please select an option: ");
        
        return true;
    }
    
    public boolean displayBookAppointmentView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                   Book  Appointments                                                  ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        
        return true;
    }
    
    public boolean displayUpdateAppointmentView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                  Update Appointments                                                  ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        
        return true;
    }
    
    public String displayPetField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Pet (please select an option from the list): ");
        String petOwnerSelection = scanner.nextLine();
        System.out.println();
        
        return petOwnerSelection;
    }
     
    public String displayPetSitterField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Pet Sitter (please select an option from the list): ");
        String petOwnerSelection = scanner.nextLine();
        System.out.println();
        
        return petOwnerSelection;
    }
    
    public String displayPetSitterTimeSlotField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Time Slot (please select an option from the list): ");
        String petSitterTimeSlotSelection = scanner.nextLine();
        
        System.out.println();
        
        return petSitterTimeSlotSelection;
    }
    
    public String displayRequestedServiceField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Requested Service: ");
        String petCategory = scanner.nextLine();
        
        return petCategory;
    }
    
    public String displayAppointmentField()
    {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Appointment (please select an option from the list): ");
        String appointmentSelection = scanner.nextLine();
        
        System.out.println();
        
        return appointmentSelection;
    }
    
    public String displayUpdateOptions()
    {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("What status would you like the appointment to be?");
        System.out.println("1. Attended");
        System.out.println("2. Unattended");
        System.out.println("3. Cancelled");
        System.out.println();
        System.out.print("Please select an option: ");        
        String statusSelection = scanner.nextLine();
        
        System.out.println();
        
        return statusSelection;
    }
    
    public void displayNoDataMessage()
    {
        System.out.println("Sorry! Appointment cannot be booked. Need at least one pet and one pet sitter.");
    }
    
    public void displayNoAppointmentMessage()
    {
        System.out.println("Sorry! Appointment cannot be updated. Need at least one appointment.");
    }
    
    public void displayCannotBookMessage()
    {
        System.out.println("Sorry! Appointment cannot be booked. Pet Sitter is not available.");
    }
}