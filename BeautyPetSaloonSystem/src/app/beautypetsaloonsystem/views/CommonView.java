package app.beautypetsaloonsystem.views;

import java.util.Scanner;

/**
 *
 * @author KishaniJoseph
 */
public class CommonView {
    public String displayNameField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Name: ");
        String name = scanner.nextLine();
        
        return name;
    }
    
    public String displayDateField()
    {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Date of Birth (dd/mm/yyyy): ");
        String dateOfBirth = scanner.nextLine();
        
        return dateOfBirth;   
    }
    
    public String displayContactNoField()
    {
         Scanner scanner = new Scanner(System.in);
        
        System.out.print("Contact No: ");
        String contactNo = scanner.nextLine();
        
        return contactNo;       
    } 
}
