package app.beautypetsaloonsystem.views;

import java.util.Scanner;

/**
 *
 * @author Kishani Joseph
 */
public class ProductOrderView {
    public boolean displayProductOrderView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                     Order Products                                                    ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------"); 
        
        return true;
    }
    
    public String displayPetProductField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Pet Product (please select an option from the list): ");
        String name = scanner.nextLine();
        
        return name;
    }
    
    public String displayOrderQuantityField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Order Quantity: ");
        String name = scanner.nextLine();
        
        return name;
    }
    
    public void displayNoDataMessage()
    {
        System.out.println("Sorry! Products cannot be ordered. Need at least one pet product.");
    }
}
