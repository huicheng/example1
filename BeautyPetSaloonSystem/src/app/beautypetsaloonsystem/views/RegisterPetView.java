package app.beautypetsaloonsystem.views;

import java.util.Scanner;

/**
 *
 * @author Kishani Joseph
 */
public class RegisterPetView {
    public boolean displayRegisterPetView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                     Register Pet                                                      ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------"); 
        
        return true;
    }
    
    public String displayPetNameField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Pet Name: ");
        String petCategory = scanner.nextLine();
        
        return petCategory;
    }
    
    public String displayPetCategoryField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Pet Category: ");
        String petCategory = scanner.nextLine();
        
        return petCategory;
    }
    
    public String displayPetBreedField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Pet Breed: ");
        String petBreed = scanner.nextLine();
        
        return petBreed;
    }
    
    public String displayPetOwnerField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Pet Owner (please select an option from the list): ");
        String petOwnerSelection = scanner.nextLine();
        System.out.println();
        
        return petOwnerSelection;
    }
    
    public void displayNoDataMessage()
    {
        System.out.println("Sorry! Pet cannot be registered. Need at least one pet owner.");
    }
}
