package app.beautypetsaloonsystem.views;

/**
 *
 * @author Kishani Joseph
 */
public class ContinueView {    
    public boolean displayContinueView(String continueMessage)
    {
        System.out.println();
        System.out.print(continueMessage);
        
        return true;
    }
}
