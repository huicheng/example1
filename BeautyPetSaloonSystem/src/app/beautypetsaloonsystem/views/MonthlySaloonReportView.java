package app.beautypetsaloonsystem.views;

/**
 *
 * @author Kishani Joseph
 */
public class MonthlySaloonReportView {
    public boolean displayMonthlySaloonReportView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                           Beauty Pet Saloon Monthly Report                                            ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------"); 
        
        return true;
    }      
}
