package app.beautypetsaloonsystem.views;

import java.util.Scanner;

/**
 *
 * @author Kishani Joseph
 */
public class ManagePetTreatmentView {
    public boolean displayManagePetTreatmentView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                 Manage Pet Treatment                                                  ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------"); 
        System.out.println("What would you like to do?");
        System.out.println("1. Add Pet Products in Pet Treatment");
        System.out.println("2. Prescribe Pet Treatment");
        System.out.println("98. Home");
        System.out.println("99. Exit");
        System.out.println();
        System.out.print("Please select an option: ");
        
        return true;
    }
    
    public boolean displayRegisterPetTreatmentView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                Register Pet Treatment                                                 ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------"); 
        
        return true;
    }
    
    
    public boolean displayAddPetProductsForTreatmentView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                           Add Pet Products in Pet Treatment                                           ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------"); 
        
        return true;
    }
    
    public boolean displayPrescribePetTreatmentView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                Prescribe Pet Treatment                                                ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------"); 
        
        return true;
    }
    
    public String displayTreatmentNameField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Treatment Name: ");
        String petBreed = scanner.nextLine();
        
        return petBreed;
    }
    
    public String displayPetTreatmentField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Pet Treatment (please select an option from the list): ");
        String petOwnerSelection = scanner.nextLine();
        System.out.println();
        
        return petOwnerSelection;
    }
    
    public String displayPetProductField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Pet Product (please select an option from the list): ");
        String petOwnerSelection = scanner.nextLine();
        System.out.println();
        
        return petOwnerSelection;
    }
    
    public String displayPetField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Pet (please select an option from the list): ");
        String petOwnerSelection = scanner.nextLine();
        System.out.println();
        
        return petOwnerSelection;
    }
    
    public void displayNoDataMessageForAddProducts()
    {
        System.out.println("Sorry! Product cannot be added to pet treatment. Need at least one pet product and one pet treatment.");
    }
    
    public void displayNoDataMessageForPrescribeTreatments()
    {
        System.out.println("Sorry! Pet treatment cannot be prescribed to pet. Need at least one pet and one pet treatment.");
    }
    
    public void displayProductDuplicateMessage()
    {
        System.out.println("Sorry! Product has already been added to pet treatment.");
    }
    
    public void displayPetTreatmentDuplicateMessage()
    {
        System.out.println("Sorry! Pet Treatment has already been prescribed to pet.");
    }
}
