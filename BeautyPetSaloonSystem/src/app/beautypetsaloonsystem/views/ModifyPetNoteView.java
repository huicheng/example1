package app.beautypetsaloonsystem.views;

import java.util.Scanner;

/**
 *
 * @author Kishani Joseph
 */
public class ModifyPetNoteView {
    public boolean displayModifyPetNoteView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                    Modify Pet Note                                                    ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------"); 
        
        return true;
    }
    
    public String displayPetField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Pet (please select an option from the list): ");
        String petOwnerSelection = scanner.nextLine();
        System.out.println();
        
        return petOwnerSelection;
    }
    
    public String displayPetNoteField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Pet Note: ");
        String petCategory = scanner.nextLine();
        
        return petCategory;
    }
    
    public void displayNoDataMessage()
    {
        System.out.println("Sorry! Pet note cannot be modified. Need at least one pet.");
    }    
}
