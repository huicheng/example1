package app.beautypetsaloonsystem.views;

/**
 *
 * @author Kishani Joseph
 */
public class HomePageView {
    public boolean displayHomePageView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                             Beauty Pet Saloon System Home                                             ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        
        System.out.println("What would you like to do?");
        System.out.println("1. Manage Appointments");
        System.out.println("2. Assign Pet to Pet Sitter");
        System.out.println("3. Register Information");
        System.out.println("4. Manage Pet Treatments");        
        System.out.println("5. Order Products");
        System.out.println("6. Modify Pet Note");
        System.out.println("7. Retrieve Current Product List");
        System.out.println("8. Monthly Report");        
        System.out.println("99. Exit");
        System.out.println();
        System.out.print("Please select an option: ");
        
        return true;
    }    
}
