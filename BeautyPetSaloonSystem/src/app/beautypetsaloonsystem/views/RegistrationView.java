package app.beautypetsaloonsystem.views;

/**
 *
 * @author Kishani Joseph
 */
public class RegistrationView {
    public boolean displayRegistrationView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                  Registration Page                                                 ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        
        System.out.println("What information would you like to register?");
        System.out.println("1. Pet Sitter");
        System.out.println("2. Pet Owner");
        System.out.println("3. Pet");        
        System.out.println("4. Pet Product");        
        System.out.println("5. Pet Treatment");
        System.out.println("98. Home");
        System.out.println("99. Exit");
        System.out.println();
        System.out.print("Please select an option: ");
        
        return true;           
    }    
}
