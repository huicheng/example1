package app.beautypetsaloonsystem.views;

import java.util.Scanner;

/**
 *
 * @author Kishani Joseph
 */
public class RegisterPetSitterView {
    public boolean displayRegisterPetSitterView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                  Register Pet Sitter                                                  ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------"); 
        
        return true;
    }
    
    public String displayDesignationField()
    {               
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Designation: ");
        String name = scanner.nextLine();
        
        return name;
    }
}
