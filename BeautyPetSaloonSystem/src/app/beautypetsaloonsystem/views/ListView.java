package app.beautypetsaloonsystem.views;

/**
 *
 * @author Kishani Joseph
 */
public class ListView {
    public boolean displayCurrentProductListView()
    {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------");
        System.out.println("                                                 Current Products in Use                                               ");
        System.out.println("                                                 - For Shop Keeper Use -                                               ");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------"); 
        
        return true;
    }
    
    public void displayCurrentProductNoDataMessage()
    {
        System.out.println("Sorry! Current products cannot be viewed. Need at least one pet, one pet treatment, and one product");
    }
}
